const user = process.env.MONGO_USER
const pass = process.env.MONGO_PASSWORD
const secretOrKey = process.env.SECRET_OR_KEY

module.exports = {
  mongoURI: `mongodb://${user}:${pass}@localhost:27017/dev`,
  secretOrKey,
}
