// NPM Modules
const JwtStrategy = require('passport-jwt').Strategy
const { ExtractJwt } = require('passport-jwt')

// File Modules
const { secretOrKey } = require('../config/keys')
const User = require('../models/User')

const options = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey,
}

module.exports = passport => {
  passport.use(
    new JwtStrategy(options, async (jwtPayload, done) => {
      let user
      try {
        // Find user by id
        user = await User.findById(jwtPayload.id)
      } catch (error) {
        console.log(error.message)
      }

      // If user is not found, return false
      if (!user) return done(null, false)
      // Return found user object
      return done(null, user)
    })
  )
}
