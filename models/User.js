// NPM Modules
const mongoose = require('mongoose')

const { Schema } = mongoose

// Create Schema
const schema = new Schema({
  name: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  avatar: {
    type: String,
  },
  date: {
    type: Date,
    default: Date.now,
  },
})

// Create Model
const User = mongoose.model('users', schema)

module.exports = User
