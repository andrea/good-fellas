// NPM Modules
const { isEmpty } = require('lodash')
const Validator = require('validator')

// File Modules
const initProps = require('./init')

module.exports = function validateRegisterInput(data, props) {
  const errors = {}

  // Pick required inputs and convert them to strings
  const d = initProps(data, props)

  // Validation checks
  if (Validator.isEmpty(d.name)) {
    errors.name = 'Name field is required.'
  } else if (!Validator.isLength(d.name, { min: 3, max: 128 })) {
    errors.name = 'Name must be between 3 and 128 characters.'
  }

  if (Validator.isEmpty(d.email)) {
    errors.email = 'Email field is required.'
  } else if (!Validator.isEmail(d.email)) {
    errors.email = 'Email is invalid.'
  }

  if (Validator.isEmpty(d.password)) {
    errors.password = 'Password field is required.'
  } else if (!Validator.isLength(d.password, { min: 6, max: 64 })) {
    errors.password = 'Password must be between 6 and 64 characters.'
  }

  if (Validator.isEmpty(d.passwordConfirm)) {
    errors.passwordConfirm = 'Confirm password field is required.'
  } else if (!Validator.equals(d.password, d.passwordConfirm)) {
    errors.passwordConfirm = 'Passwords must match.'
  }

  return {
    errors,
    isValid: isEmpty(errors),
  }
}
