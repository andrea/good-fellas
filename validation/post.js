// NPM Modules
const { isEmpty } = require('lodash')
const Validator = require('validator')

// File Modules
const initProps = require('./init')

module.exports = function validatePostInput(data, props) {
  const errors = {}

  // Pick required inputs and convert them to strings
  const d = initProps(data, props)

  // Validation checks
  if (Validator.isEmpty(d.text)) {
    errors.text = 'Text field is required.'
  } else if (!Validator.isLength(d.text, { min: 10, max: 300 })) {
    errors.text = 'Post must be between 10 and 300 characters.'
  }

  return {
    errors,
    isValid: isEmpty(errors),
  }
}
