/* eslint-disable security/detect-object-injection */

// NPM Modules
const { flatMap, isEmpty } = require('lodash')
const Validator = require('validator')

// File Modules
const initProps = require('./init')

module.exports = function validateProfileInput(data, props) {
  const errors = {}

  // Pick required inputs and convert them to strings
  const d = initProps(data, props)

  if (Validator.isEmpty(d.handle)) {
    errors.handle = 'Profile handle field is required.'
  } else if (!Validator.isLength(d.handle, { min: 3, max: 40 })) {
    errors.handle = 'Handle needs to be between 3 and 40 characters.'
  }

  // Validation checks
  if (Validator.isEmpty(d.status)) {
    errors.status = 'Status field is required.'
  }

  if (Validator.isEmpty(d.skills)) {
    errors.skills = 'Skills field is required.'
  }

  // Grab all URLs for validation
  const urls = flatMap(props).filter(name => /URL$/.test(name))

  urls.forEach((url, index) => {
    if (!isEmpty(d[urls[index]]) && !Validator.isURL(d[urls[index]]))
      errors[urls[index]] = 'Not a valid URL.'
  })

  return {
    errors,
    isValid: isEmpty(errors),
  }
}
