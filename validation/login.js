// NPM Modules
const { isEmpty } = require('lodash')
const Validator = require('validator')

// File Modules
const initProps = require('./init')

module.exports = function validateLoginInput(data, props) {
  const errors = {}

  // Pick required inputs and convert them to strings
  const d = initProps(data, props)

  // Validation checks
  if (Validator.isEmpty(d.email)) {
    errors.email = 'Email field is required.'
  } else if (!Validator.isEmail(d.email)) {
    errors.email = 'Email is invalid.'
  }

  if (Validator.isEmpty(d.password)) {
    errors.password = 'Password field is required.'
  } else if (!Validator.isLength(d.password, { min: 6, max: 64 })) {
    errors.password = 'Password must be between 6 and 64 characters.'
  }

  return {
    errors,
    isValid: isEmpty(errors),
  }
}
