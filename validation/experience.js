// NPM Modules
const { isEmpty } = require('lodash')
const Validator = require('validator')

// File Modules
const initProps = require('./init')

module.exports = function validateExperienceInput(data, props) {
  const errors = {}

  // Pick required inputs and convert them to strings
  const d = initProps(data, props)

  // Validation checks
  if (Validator.isEmpty(d.title)) {
    errors.title = 'Job title field is required.'
  }

  if (Validator.isEmpty(d.company)) {
    errors.company = 'Company field is required.'
  }

  if (Validator.isEmpty(d.from)) {
    errors.from = 'From date field is required.'
  }

  return {
    errors,
    isValid: isEmpty(errors),
  }
}
