// NPM Modules
const _ = require('lodash')

const initProps = (data, props) => {
  const d = { ...data }
  const p = _.flatMap(props)
  const defaults = p.reduce((a, v) => ({ ...a, [v]: '' }), {})

  // Pick required inputs and convert them to strings
  return _(d)
    .defaults(defaults)
    .pick(p)
    .mapValues(_.toString)
    .mapValues(_.trim)
    .value()
}

module.exports = initProps
