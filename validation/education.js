// NPM Modules
const { isEmpty } = require('lodash')
const Validator = require('validator')

// File Modules
const initProps = require('./init')

module.exports = function validateExperienceInput(data, props) {
  const errors = {}

  // Pick required inputs and convert them to strings
  const d = initProps(data, props)

  // Validation checks
  if (Validator.isEmpty(d.school)) {
    errors.school = 'School name field is required.'
  }

  if (Validator.isEmpty(d.degree)) {
    errors.degree = 'Degree field is required.'
  }

  if (Validator.isEmpty(d.from)) {
    errors.from = 'From date field is required.'
  }

  if (Validator.isEmpty(d.fieldOfStudy)) {
    errors.fieldOfStudy = 'Field of study field is required.'
  }

  return {
    errors,
    isValid: isEmpty(errors),
  }
}
