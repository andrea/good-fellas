// NPM Modules
require('dotenv').config()
const express = require('express')
const mongoose = require('mongoose')
const passport = require('passport')

// File Modules
const { mongoURI } = require('./config/keys')
const posts = require('./routes/api/posts')
const profile = require('./routes/api/profile')
const users = require('./routes/api/users')

const port = process.env.PORT || 5000
const app = express()

// DB Config
const dbOptions = {
  authSource: 'admin',
  useNewUrlParser: true,
}

// Connect to MongoDB
mongoose
  .connect(
    mongoURI,
    dbOptions
  )
  .then(() => console.log('MongoDB Connected'))
  .catch(error => console.log(error.message))

// Express middleware
app.use(express.json())
app.use(passport.initialize())

// Passport config
require('./config/passport')(passport)

// Use routes
app.use('/api/users', users)
app.use('/api/posts', posts)
app.use('/api/profile', profile)

app.listen(port, () => {
  console.log(`Server running on port ${port}`)
})
