/* eslint-disable no-underscore-dangle */

// NPM Modules
const express = require('express')
const mongoose = require('mongoose')
const passport = require('passport')
const _ = require('lodash')

// File Modules
const Post = require('../../models/Post')
const validatePostInput = require('../../validation/post')

const router = express.Router()

/**
 * GET      api/posts/test
 *
 * @desc    Tests users route
 * @access  Public
 *
 */
router.get('/test', (req, res) => {
  res.json({ msg: 'Posts Works!' })
})

/**
 * GET      api/posts
 *
 * @desc    Get all post
 * @access  Public
 *
 */
router.get('/', async (req, res) => {
  let posts

  try {
    posts = await Post.find().sort({ date: -1 })

    if (posts.length === 0)
      return res.status(404).json({ noPosts: 'No posts found.' })
  } catch ({ message }) {
    return res.status(500).json({ error: message })
  }

  // Return list of posts
  return res.json(posts)
})

/**
 * GET      api/posts/:id
 *
 * @desc    Get post by Id
 * @access  Public
 *
 */
router.get('/:id', async (req, res) => {
  const isValidId = mongoose.Types.ObjectId.isValid(req.params.id)

  // If no valid ObjectId provided, return 400.
  if (!isValidId)
    return res.status(400).json({ id: 'Provided Id is not valid.' })

  let post

  try {
    post = await Post.findById(req.params.id)

    if (!post) return res.status(404).json({ noPosts: 'Post not found.' })
  } catch ({ message }) {
    return res.status(500).json({ error: message })
  }

  // Return list of posts
  return res.json(post)
})

/**
 * POST     api/posts
 *
 * @desc    Create post
 * @access  Private
 *
 */
router.post(
  '/',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    const props = ['text', 'name', 'avatar']
    const { errors, isValid } = validatePostInput(req.body, props)

    // Check for valid input
    if (!isValid) return res.status(400).json(errors)

    // Get user id
    const user = req.user.id

    // Create new post
    const post = new Post({ ...req.body, user })

    // Save post to database
    try {
      await post.save()
    } catch ({ message }) {
      return res.status(500).json({ error: message })
    }

    // Return post
    return res.json(post)
  }
)

/**
 * DELETE   api/posts/:id
 *
 * @desc    Delete post by Id
 * @access  Private
 *
 */
router.delete(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    const isValidId = mongoose.Types.ObjectId.isValid(req.params.id)

    // If no valid ObjectId provided, return 400.
    if (!isValidId)
      return res.status(400).json({ id: 'Provided Id is not valid.' })

    try {
      const post = await Post.findById(req.params.id)

      if (!post) return res.status(404).json({ noPostFound: 'Post not found' })

      if (post.user.toString() !== req.user.id)
        return res.status(401).json({ notAuthorized: 'User not authorized.' })

      await post.remove()
    } catch ({ message }) {
      return res.status(500).json({ error: message })
    }

    return res.json({ success: true })
  }
)

/**
 * POST     api/posts/like/:id
 *
 * @desc    Like post by Id
 * @access  Private
 *
 */
router.post(
  '/like/:id',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    const isValidId = mongoose.Types.ObjectId.isValid(req.params.id)

    // If no valid ObjectId provided, return 400.
    if (!isValidId)
      return res.status(400).json({ id: 'Provided Id is not valid.' })

    let updatedPost
    try {
      const post = await Post.findById(req.params.id)

      if (!post) return res.status(404).json({ noPostFound: 'Post not found' })

      const isLiked = post.likes.findIndex(
        like => like.user.toString() === req.user.id
      )

      // Check if posts has been liked already by the current user
      if (isLiked !== -1)
        return res
          .status(400)
          .json({ alreadyLiked: 'User already liked this post.' })

      // Add user Id to likes array
      post.likes = [...post.likes, { user: req.user.id }]

      // Save updated post
      updatedPost = await post.save()
    } catch ({ message }) {
      return res.status(500).json({ error: message })
    }

    // Return post
    return res.json(updatedPost)
  }
)

/**
 * POST     api/posts/unlike/:id
 *
 * @desc    Unlike post by Id
 * @access  Private
 *
 */
router.post(
  '/unlike/:id',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    const isValidId = mongoose.Types.ObjectId.isValid(req.params.id)

    // If no valid ObjectId provided, return 400.
    if (!isValidId)
      return res.status(400).json({ id: 'Provided Id is not valid.' })

    let updatedPost
    try {
      const post = await Post.findById(req.params.id)

      if (!post) return res.status(404).json({ noPostFound: 'Post not found' })

      const isLiked = post.likes.findIndex(
        like => like.user.toString() === req.user.id
      )

      // Check if posts has been liked already by the current user
      if (isLiked === -1)
        return res
          .status(400)
          .json({ notLiked: 'You have not yet liked this post.' })

      // Remove user Id from likes array
      post.likes = post.likes.filter(
        like => like.user.toString() !== req.user.id
      )
      // Save updated post
      updatedPost = await post.save()
    } catch ({ message }) {
      return res.status(500).json({ error: message })
    }

    // Return post
    return res.json(updatedPost)
  }
)

/**
 * POST     api/posts/comment/:id
 *
 * @desc    Add comment to post by Id
 * @access  Private
 *
 */
router.post(
  '/comment/:id',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    const isValidId = mongoose.Types.ObjectId.isValid(req.params.id)

    // If no valid ObjectId provided, return 400.
    if (!isValidId)
      return res.status(400).json({ id: 'Provided Id is not valid.' })

    let updatedPost
    try {
      const post = await Post.findById(req.params.id)

      if (!post) return res.status(404).json({ postNotFound: 'Post not found' })

      const props = ['text', 'name', 'avatar']
      const { errors, isValid } = validatePostInput(req.body, props)

      // Check for valid input
      if (!isValid) return res.status(400).json(errors)

      // Get user id
      const user = { user: req.user.id }

      const comment = Object.assign({}, _.pick(req.body, props), user)

      // Add new comment to comments array
      post.comments = [...post.comments, comment]

      // Save updated post
      updatedPost = await post.save()
    } catch ({ message }) {
      return res.status(500).json({ error: message })
    }

    // Return post
    return res.json(updatedPost)
  }
)

/**
 * DELETE   api/posts/comment/:id/:commentId
 *
 * @desc    Remove comment from posts by Id
 * @access  Private
 *
 */
router.delete(
  '/comment/:id/:commentId',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    const areValidIds = [req.params.id, req.params.commentId].map(id =>
      mongoose.Types.ObjectId.isValid(id)
    )

    // If any invalid ObjectId provided, return 400.
    if (!areValidIds.every(Boolean))
      return res.status(400).json({ id: 'Provided Id is not valid.' })

    let updatedPost
    try {
      const post = await Post.findById(req.params.id)

      if (!post) return res.status(404).json({ postNotFound: 'Post not found' })

      // Check if comment exists
      const comment = post.comments.findIndex(
        c => c._id.toString() === req.params.commentId
      )

      if (comment === -1)
        return res.status(404).json({ commentNotFound: 'Comment not found.' })

      // Remove comment from comments array
      post.comments = post.comments.filter(
        c => c._id.toString() !== req.params.commentId
      )

      // Save updated post
      updatedPost = await post.save()
    } catch ({ message }) {
      return res.status(500).json({ error: message })
    }

    // Return post
    return res.json(updatedPost)
  }
)

module.exports = router
