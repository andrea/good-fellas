// NPM Modules
const express = require('express')
const mongoose = require('mongoose')
const passport = require('passport')
const _ = require('lodash')

// File Modules
const Profile = require('../../models/Profile')
const User = require('../../models/User')
const validateProfileInput = require('../../validation/profile')
const validateExperienceInput = require('../../validation/experience')
const validateEducationInput = require('../../validation/education')

const router = express.Router()

/**
 * GET      api/profile/test
 *
 * @desc    Tests profile route
 * @access  Public
 *
 */
router.get('/test', (req, res) => {
  res.json({ msg: 'Profile Works!' })
})

/**
 * GET      api/profile/handle/:handle
 *
 * @desc    Get profile by handle
 * @access  Private
 */
router.get('/handle/:handle', async (req, res) => {
  let profile

  try {
    profile = await Profile.findOne({ handle: req.params.handle }).populate(
      'user',
      ['name', 'avatar']
    )

    if (!profile)
      return res
        .status(404)
        .json({ noProfile: 'There is no profile for this user.' })
  } catch ({ message }) {
    return res.status(500).json({ error: message })
  }

  return res.json(profile)
})

/**
 * GET      api/profile/all
 *
 * @desc    Get all profiles
 * @access  Private
 */
router.get('/all', async (req, res) => {
  let profiles
  try {
    profiles = await Profile.find().populate('user', ['name', 'avatar'])

    if (profiles.length === 0)
      return res.status(404).json({ noProfile: 'There are no profiles.' })

    return res.json(profiles)
  } catch ({ message }) {
    return res.status(500).json({ error: message })
  }
})

/**
 * GET      api/profile/user/:id
 *
 * @desc    Get profile by handle
 * @access  Private
 */
router.get('/user/:id', async (req, res) => {
  const isValidId = mongoose.Types.ObjectId.isValid(req.params.id)

  // If no valid ObjectId provided, return 400.
  if (!isValidId)
    return res.status(400).json({ id: 'Provided Id is not valid.' })

  let profile
  try {
    profile = await Profile.findOne({ user: req.params.id }).populate('user', [
      'name',
      'avatar',
    ])

    if (!profile)
      return res
        .status(404)
        .json({ noProfile: 'There is no profile for this user.' })
  } catch ({ message }) {
    return res.status(500).json({ error: message })
  }

  return res.json(profile)
})

/**
 * GET      api/profile
 *
 * @desc    Get current user's profile
 * @access  Private
 */
router.get(
  '/',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    let profile

    try {
      // Find user profile
      profile = await Profile.findOne({ user: req.user.id }).populate('user', [
        'name',
        'avatar',
      ])

      // If no profile found, return 404
      if (!profile)
        return res
          .status(404)
          .json({ noProfile: 'There is no profile for this user.' })
    } catch ({ message }) {
      return res.status(500).json({ error: message })
    }

    return res.json(profile)
  }
)

/**
 * POST     api/profile
 *
 * @desc    Create or edit current user's profile
 * @access  Private
 */
router.post(
  '/',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    const props = {
      info: [
        'bio',
        'company',
        'githubUserName',
        'handle',
        'location',
        'skills',
        'status',
        'websiteURL',
      ],
      social: [
        'facebookURL',
        'instagramURL',
        'linkedinURL',
        'twitterURL',
        'youtubeURL',
      ],
    }

    const { errors, isValid } = validateProfileInput(req.body, props)

    // If input is invalid, return 400
    if (!isValid) return res.status(400).json(errors)

    // Get user's id
    const user = { user: req.user.id }

    // Get user's info
    const info = _.pick(req.body, props.info)

    // Get user' skills
    let skills = []
    if (typeof req.body.skills === 'string') {
      skills = req.body.skills.split(',').map(s => s.trim())
    }

    // Get user' social media profiles
    const social = _.pick(req.body, props.social)

    // Build profileFields object
    const profileFields = Object.assign({}, user, info, { skills }, { social })

    let profile

    // Find an existing profile for current user
    try {
      profile = await Profile.findOne(user)
    } catch ({ message }) {
      return res.status(500).json({ error: message })
    }

    // Update current user profile if it already exists
    if (profile) {
      // Profile exists, then update and save it
      const updatedProfile = await Profile.findOneAndUpdate(
        user,
        { $set: profileFields },
        { new: true }
      )

      // Return updated profile
      return res.json(updatedProfile)
    }

    // If a profile does not exist already, create one
    try {
      // Check if handle exists before even trying
      const existingUserProfile = await Profile.findOne({
        handle: profileFields.handle,
      })
      if (existingUserProfile)
        return res.status(400).json({ handle: 'Handle already exists.' })

      // Save new profile
      profile = await new Profile(profileFields).save()
    } catch ({ message }) {
      res.status(500).json({ error: message })
    }

    // Return new profile
    return res.json(profile)
  }
)

/**
 * POST     api/profile/experience
 *
 * @desc    Add experience to profile
 * @access  Private
 */
router.post(
  '/experience',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    const props = [
      'company',
      'isCurrent',
      'description',
      'from',
      'location',
      'title',
      'to',
    ]

    const { errors, isValid } = validateExperienceInput(req.body, props)

    // Check for valid input
    if (!isValid) return res.status(400).json(errors)

    let updatedProfile
    try {
      const profile = await Profile.findOne({ user: req.user.id })

      const newExp = _.pick(req.body, props)

      // Update profile
      profile.experience = [...profile.experience, newExp]

      // Save updated profile
      updatedProfile = await profile.save()
    } catch ({ message }) {
      return res.status(500).json({ error: message })
    }

    // Return updated user profile
    return res.json(updatedProfile)
  }
)

/**
 * POST     api/profile/education
 *
 * @desc    Add education to profile
 * @access  Private
 */
router.post(
  '/education',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    const props = [
      'isCurrent',
      'degree',
      'description',
      'fieldOfStudy',
      'from',
      'school',
      'to',
    ]

    const { errors, isValid } = validateEducationInput(req.body, props)

    // Check for valid input
    if (!isValid) return res.status(400).json(errors)

    let updatedProfile
    try {
      const profile = await Profile.findOne({ user: req.user.id })

      const newEdu = _.pick(req.body, props)

      // Update profile
      profile.education = [...profile.education, newEdu]

      // Save updated profile
      updatedProfile = await profile.save()
    } catch ({ message }) {
      return res.status(500).json({ error: message })
    }

    // Return updated user profile
    return res.json(updatedProfile)
  }
)

/**
 * DELETE   api/profile/experience/:id
 *
 * @desc    Delete experience entry from profile
 * @access  Private
 */
router.delete(
  '/experience/:id',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    const isValidId = mongoose.Types.ObjectId.isValid(req.params.id)

    // If no valid ObjectId provided, return 400.
    if (!isValidId)
      return res.status(400).json({ id: 'Provided Id is not valid.' })

    let updatedProfile
    try {
      const profile = await Profile.findOne({ user: req.user.id })

      if (!profile)
        return res
          .status(404)
          .json({ noExperience: 'There is no experience entry with this Id.' })

      // Remove experience entry at index
      profile.experience = profile.experience.filter(
        exp => exp.id !== req.params.id
      )

      // Save updated profile
      updatedProfile = await profile.save()
    } catch ({ message }) {
      return res.status(500).json({ error: message })
    }

    // Return updated user profile
    return res.json(updatedProfile)
  }
)

/**
 * DELETE   api/profile/education/:id
 *
 * @desc    Delete education entry from profile
 * @access  Private
 */
router.delete(
  '/education/:id',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    const isValidId = mongoose.Types.ObjectId.isValid(req.params.id)

    // If no valid ObjectId provided, return 400.
    if (!isValidId)
      return res.status(400).json({ id: 'Provided Id is not valid.' })

    let updatedProfile
    try {
      const profile = await Profile.findOne({ user: req.user.id })

      if (!profile)
        return res
          .status(404)
          .json({ noEducation: 'There is no education entry with this Id.' })

      // Remove education entry at index
      profile.education = profile.education.filter(
        edu => edu.id !== req.params.id
      )

      // Save updated profile
      updatedProfile = await profile.save()
    } catch ({ message }) {
      return res.status(500).json({ error: message })
    }

    // Return updated user profile
    return res.json(updatedProfile)
  }
)

/**
 * DELETE   api/profile
 *
 * @desc    Delete user and profile
 * @access  Private
 */
router.delete(
  '/',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    const isValidId = mongoose.Types.ObjectId.isValid(req.user.id)

    // If no valid ObjectId provided, return 400.
    if (!isValidId)
      return res.status(400).json({ id: 'Provided Id is not valid.' })

    try {
      const profile = await Profile.findOneAndRemove({ user: req.user.id })

      if (!profile)
        return res
          .status(404)
          .json({ noProfile: 'No profile found with the provided Id.' })

      const user = await User.findOneAndRemove({ _id: req.user.id })

      if (!user)
        return res
          .status(404)
          .json({ noUser: 'No user found with the provided Id.' })
    } catch ({ message }) {
      return res.status(500).json({ error: message })
    }

    return res.json({ success: true })
  }
)

module.exports = router
