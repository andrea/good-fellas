// Core Modules
const { promisify } = require('util')

// NPM Modules
const bcrypt = require('bcryptjs')
const express = require('express')
const gravatar = require('gravatar')
const jwt = require('jsonwebtoken')
const passport = require('passport')
const _ = require('lodash')

// File Modules
const User = require('../../models/User')
const { secretOrKey } = require('../../config/keys')
const validateLoginInput = require('../../validation/login')
const validateRegisterInput = require('../../validation/register')

const router = express.Router()
const jwtSign = promisify(jwt.sign)

/**
 * GET      api/users/test
 *
 * @desc    Test users route
 * @access  Public
 *
 */
router.get('/test', (req, res) => {
  res.json({ msg: 'Users Works!' })
})

/**
 * POST     api/users/login
 *
 * @desc    Login user and return JWT
 * @access  Public
 *
 */

router.post('/login', async (req, res) => {
  const props = ['email', 'password']

  const { errors, isValid } = validateLoginInput(req.body, props)

  // Check for valid input
  if (!isValid) return res.status(400).json(errors)

  const { email, password } = req.body
  let token

  // Find user by email
  const user = await User.findOne({ email })

  // If user does not exist, return 404
  if (!user) return res.status(404).json({ email: 'User not found' })

  try {
    // Check password
    const isMatch = await bcrypt.compare(password, user.password)

    // If password is incorrect, return 400
    if (!isMatch)
      return res.status(400).json({ password: 'Password incorrect' })

    // Set payload object
    const payload = _.pick(user, ['id', 'name', 'avatar'])

    // Sign Token with promisified version of jwt.sign
    token = await jwtSign(payload, secretOrKey, { expiresIn: '1h' })
  } catch ({ message }) {
    // If the server errors out, send back its description
    return res.status(500).json({ error: message })
  }

  // Respond with generated JWT and return
  return res.json({ success: true, token: `Bearer ${token}` })
})

/**
 * POST     api/users/register
 *
 * @desc    Register new user
 * @access  Public
 */
router.post('/register', async (req, res) => {
  const props = ['name', 'email', 'password', 'passwordConfirm']

  const { errors, isValid } = validateRegisterInput(req.body, props)

  // Check for valid input
  if (!isValid) return res.status(400).json(errors)

  // Find user object in database
  let user = await User.findOne({ email: req.body.email })

  // If user already exists, return 400
  if (user) return res.status(400).json({ email: 'Email already exists' })

  try {
    // Get data from req.body.
    const { name, email, password } = req.body

    // Get the gravatar url.
    const avatar = gravatar.url(email, { s: '200', r: 'pg', d: 'mm' })

    // Create new user.
    const newUser = new User({ name, email, password, avatar })

    // Generate salt, then hash user's password, then save user.
    const salt = await bcrypt.genSalt(10)

    // Replace plain-text password with hashed password
    newUser.password = await bcrypt.hash(newUser.password, salt)

    // Save new user
    user = await newUser.save()
  } catch ({ message }) {
    // If the server errors out, send back its description
    return res.status(500).json({ error: message })
  }

  // Send back new user and return
  return res.json(_.pick(user, ['id', 'email']))
})

/**
 * GET      api/users/current
 *
 * @desc    Return current user details
 * @access  Private
 */
router.get(
  '/current',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const user = _.pick(req.user, ['id', 'name', 'email'])

    // Send back user details
    return res.json(user)
  }
)

module.exports = router
