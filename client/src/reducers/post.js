/* eslint-disable no-underscore-dangle */
import {
  ADD_POST,
  DELETE_POST,
  GET_POST,
  GET_POSTS,
  POST_LOADING,
} from '../types'

const initialState = {
  posts: [],
  post: {},
  loading: false,
}

const post = (state = initialState, action) => {
  switch (action.type) {
    case POST_LOADING:
      return { ...state, loading: true }
    case ADD_POST:
      if (state.posts === null) return { ...state, posts: [action.payload] }
      return { ...state, posts: [action.payload, ...state.posts] }
    case GET_POSTS:
      return { ...state, posts: action.payload, loading: false }
    case GET_POST:
      return { ...state, post: action.payload, loading: false }
    case DELETE_POST:
      return {
        ...state,
        posts: state.posts.filter(p => p._id !== action.payload),
      }
    default:
      return state
  }
}

export default post
