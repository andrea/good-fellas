/* eslint-disable class-methods-use-this */
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'

class Landing extends Component {
  componentDidMount() {
    if (this.props.auth.isAuthenticated) {
      this.props.history.push('/dashboard')
    }
  }

  render() {
    return (
      <main className="landing hero">
        <div className="hero-body">
          <div className="container has-text-centered">
            <h1 className="title is-1 has-text-light">The Good Fellas</h1>
            <p className="subtitle is-6 has-text-light">
              Create some sort of profile, where you can share stuff.
            </p>
            <div className="hero-buttons is-centered">
              <Link
                className="button is-medium is-outlined is-light"
                to="/register"
              >
                Sign Up
              </Link>
              <Link
                className="button is-medium is-outlined is-light"
                to="/login"
              >
                Login
              </Link>
            </div>
          </div>
        </div>
      </main>
    )
  }
}

Landing.propTypes = {
  auth: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
}

const mapStateToProps = ({ auth }) => ({ auth })

export default connect(mapStateToProps)(Landing)
