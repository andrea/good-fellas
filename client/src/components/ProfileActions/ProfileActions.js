import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import classNames from 'classnames'
import { connect } from 'react-redux'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { deleteAccount } from '../../actions/profile'

class ProfileActions extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isActive: false,
    }

    this.handleDeleteAccount = this.handleDeleteAccount.bind(this)
    this.handleModalVisibilityClick = this.handleModalVisibilityClick.bind(this)
    this.handleModalVisibilityKeyDown = this.handleModalVisibilityKeyDown.bind(
      this
    )
  }

  componentDidMount() {
    document.addEventListener('keydown', this.handleModalVisibilityKeyDown)
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.handleModalVisibilityKeyDown)
  }

  handleDeleteAccount() {
    this.props.deleteAccount()
  }

  handleModalVisibilityClick(e) {
    e.stopPropagation()
    this.setState(s => ({ isActive: !s.isActive }))
  }

  handleModalVisibilityKeyDown(event) {
    if (event.keyCode === 27) {
      this.setState(() => ({ isActive: false }))
    }
  }

  render() {
    return (
      <section className="info-tiles profile-actions">
        <div className="tile is-ancestor has-text-centered">
          <div className="tile is-parent">
            <article className="tile is-child">
              <Link to="/add-experience" className="box">
                <figure className="profile-action">
                  <FontAwesomeIcon
                    icon="check-circle"
                    className="icon is-large"
                  />
                  <figcaption className="subtitle has-text-weight-bold">
                    Add Experience
                  </figcaption>
                </figure>
              </Link>
            </article>
          </div>
          <div className="tile is-parent">
            <article className="tile is-child">
              <Link to="/add-education" className="box">
                <figure className="profile-action">
                  <FontAwesomeIcon icon="shapes" className="icon is-large" />
                  <figcaption className="subtitle has-text-weight-bold">
                    Add Education
                  </figcaption>
                </figure>
              </Link>
            </article>
          </div>
          <div className="tile is-parent">
            <article className="tile is-child">
              <Link to="/edit-profile" className="box">
                <figure className="profile-action">
                  <FontAwesomeIcon
                    icon="user-circle"
                    className="icon is-large"
                  />
                  <figcaption className="subtitle has-text-weight-bold">
                    Edit Profile
                  </figcaption>
                </figure>
              </Link>
            </article>
          </div>
          <div className="tile is-parent">
            <article className="tile is-child">
              <a
                className="box action-delete has-text-danger"
                onClick={this.handleModalVisibilityClick}
              >
                <figure className="profile-action">
                  <FontAwesomeIcon icon="skull" className="icon is-large" />
                  <figcaption className="subtitle has-text-weight-bold has-text-danger">
                    Delete Account
                  </figcaption>
                </figure>
              </a>
            </article>
          </div>
        </div>

        <div
          className={classNames('modal', { 'is-active': this.state.isActive })}
        >
          <div
            className="modal-background"
            onClick={this.handleModalVisibilityClick}
          />
          <div className="modal-card">
            <header className="modal-card-head">
              <p className="modal-card-title">Warning</p>
              <button
                className="delete"
                aria-label="close"
                onClick={this.handleModalVisibilityClick}
              />
            </header>
            <section className="modal-card-body">
              <div className="content">
                <h1>Permanent Account Deletion</h1>
                <p>
                  You are about to{' '}
                  <em>delete all data related to your account</em> from the
                  database. Both your profile and credentials will be deleted.
                  Please double check this is exactly what you intend to do.{' '}
                  <strong>This operation cannot be undone</strong>.
                </p>
              </div>
            </section>
            <footer className="modal-card-foot">
              <button
                className="button is-danger"
                onClick={this.handleDeleteAccount}
              >
                Delete Account
              </button>
              <button
                className="button"
                onClick={this.handleModalVisibilityClick}
              >
                Cancel
              </button>
            </footer>
          </div>
        </div>
      </section>
    )
  }
}

ProfileActions.propTypes = {
  deleteAccount: PropTypes.func.isRequired,
}

const mapDispatchToProps = dispatch => ({
  deleteAccount: () => dispatch(deleteAccount()),
})

export default connect(
  null,
  mapDispatchToProps
)(ProfileActions)
