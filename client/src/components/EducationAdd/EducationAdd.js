import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link, withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import _ from 'lodash'

import { addEducation } from '../../actions/profile'

import InputField from '../common/InputField'
import TextAreaField from '../common/TextAreaField'
import Main from '../common/Main'

class EducationAdd extends Component {
  constructor(props) {
    super(props)

    this.state = {
      education: {
        school: '',
        degree: '',
        fieldOfStudy: '',
        from: '',
        to: '',
        isCurrent: false,
        description: '',
      },
      errors: {},
    }

    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.handleCheckbox = this.handleCheckbox.bind(this)
  }

  static getDerivedStateFromProps(nextProps) {
    if (!_.isEmpty(nextProps.errors)) {
      return {
        errors: nextProps.errors,
      }
    }

    // Return null to indicate no change to state.
    return null
  }

  handleChange(e) {
    e.persist()
    this.setState(s => ({
      education: { ...s.education, [e.target.name]: e.target.value },
    }))
  }

  handleCheckbox() {
    this.setState(s => ({
      education: {
        ...s.education,
        isCurrent: !s.education.isCurrent,
      },
    }))
  }

  handleSubmit(e) {
    e.preventDefault()
    this.props.addEducation(this.state.education, this.props.history)
  }

  render() {
    const { errors, education } = this.state

    return (
      <Main
        mainClasses="add-education"
        headerClasses="has-text-centered"
        sectionClasses="column is-8-tablet is-offset-2-tablet is-12-mobile"
        title="Add Education"
        subtitle="Add any school or other learning experience that you have attended."
      >
        <form noValidate onSubmit={this.handleSubmit}>
          <InputField
            id="school"
            name="school"
            label="School"
            placeholder="School Name"
            value={education.school}
            onChange={this.handleChange}
            error={errors.school}
            info="Enter the name of your school or institution."
            icon="school"
            isFullWidth
            isRequired
            isInputLarge
          />

          <InputField
            id="degree"
            name="degree"
            label="Degree of Certification"
            placeholder="Degree of certification"
            value={education.degree}
            onChange={this.handleChange}
            error={errors.degree}
            info="Your degree."
            icon="graduation-cap"
            isFullWidth
            isRequired
            isInputLarge
          />

          <InputField
            id="fieldOfStudy"
            name="fieldOfStudy"
            label="Field of Study"
            placeholder="Field of study"
            value={education.fieldOfStudy}
            onChange={this.handleChange}
            error={errors.fieldOfStudy}
            info="Field of Study"
            icon="star"
            isFullWidth
            isRequired
            isInputLarge
          />

          <InputField
            id="from"
            name="from"
            label="From Date"
            type="date"
            placeholder="1970-01-01"
            value={education.from}
            onChange={this.handleChange}
            error={errors.from}
            info="Starting from..."
            icon="calendar-alt"
            isFullWidth
            isRequired
            isInputLarge
          />

          <InputField
            id="to"
            name="to"
            label="To Date"
            type="date"
            value={education.to}
            onChange={this.handleChange}
            error={errors.to}
            info="Util..."
            icon="calendar-check"
            isFullWidth
            isInputLarge
            disabled={education.isCurrent}
          />

          <div className="field">
            <div className="control level">
              <label className="checkbox level-left" htmlFor="current">
                <input
                  id="current"
                  type="checkbox"
                  name="current"
                  value={education.isCurrent}
                  checked={education.isCurrent}
                  onChange={this.handleCheckbox}
                />
                <span className="level-left" style={{ marginLeft: '0.5rem' }}>
                  Currently Involved
                </span>
              </label>
            </div>
          </div>

          <TextAreaField
            placeholder="Program description..."
            name="description"
            id="description"
            label="Program Description"
            isLabelVisible={false}
            value={education.description}
            onChange={this.handleChange}
            error={errors.description}
            info="Tell us about the program you were in."
            isInputLarge
          />

          <div className="section buttons is-centered">
            <button className="button is-link is-large" type="submit">
              Add Education
            </button>
            <Link to="/dashboard" className="button is-large">
              Cancel
            </Link>
          </div>
        </form>
      </Main>
    )
  }
}

EducationAdd.propTypes = {
  profile: PropTypes.object,
  errors: PropTypes.object,
  history: PropTypes.object,
  addEducation: PropTypes.func.isRequired,
}

const mapStateToProps = () => ({ profile, errors }) => ({ profile, errors })

const mapDispatchToProps = dispatch => ({
  addEducation: (eduData, history) => dispatch(addEducation(eduData, history)),
})

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(EducationAdd)
)
