import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import _ from 'lodash'

import { registerUser } from '../../actions/auth'

// Components
import Main from '../common/Main'
import InputField from '../common/InputField'

class Register extends Component {
  constructor(props) {
    super(props)
    this.state = {
      user: {
        name: '',
        email: '',
        password: '',
        passwordConfirm: '',
      },
      errors: {},
    }

    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  static getDerivedStateFromProps(nextProps) {
    if (!_.isEmpty(nextProps.errors)) {
      return { errors: nextProps.errors }
    }

    // Return null to indicate no change to state.
    return null
  }

  componentDidMount() {
    if (this.props.auth.isAuthenticated) {
      this.props.history.push('/dashboard')
    }
  }

  handleChange(e) {
    e.persist()
    this.setState(s => ({
      user: { ...s.user, [e.target.name]: e.target.value },
    }))
  }

  handleSubmit(e) {
    e.preventDefault()
    this.props.registerUser(this.state.user, this.props.history)
  }

  render() {
    const { errors } = this.state

    return (
      <Main
        mainClasses="register"
        headerClasses="has-text-centered"
        title="Sign Up"
        subtitle="Create your Profilr account."
        sectionClasses="column is-8-tablet is-offset-2-tablet is-12-mobile"
      >
        <form noValidate onSubmit={this.handleSubmit}>
          <InputField
            id="name"
            name="name"
            label="Name"
            placeholder="John Doe"
            value={this.state.user.name}
            onChange={this.handleChange}
            icon="user-circle"
            error={errors.name}
            isFullWidth
            isRequired
            isInputLarge
          />
          <InputField
            id="email"
            name="email"
            label="Email"
            type="email"
            placeholder="hello@domain.com"
            value={this.state.user.email}
            onChange={this.handleChange}
            icon="at"
            error={errors.email}
            isFullWidth
            isRequired
            isInputLarge
          />
          <InputField
            id="password"
            name="password"
            label="Password"
            type="password"
            autocomplete="new-password"
            placeholder="password"
            value={this.state.user.password}
            onChange={this.handleChange}
            icon="lock"
            error={errors.password}
            isFullWidth
            isRequired
            isInputLarge
          />
          <InputField
            id="passwordConfirm"
            name="passwordConfirm"
            label="Confirm Password"
            type="password"
            autocomplete="new-password"
            placeholder="confirm password"
            value={this.state.user.passwordConfirm}
            onChange={this.handleChange}
            icon="lock"
            error={errors.passwordConfirm}
            isFullWidth
            isRequired
            isInputLarge
          />
          <div className="field">
            <div className="control">
              <label className="label is-invisible">Create Account</label>
              <button
                type="submit"
                className="button is-link is-large is-fullwidth"
              >
                Submit
              </button>
            </div>
          </div>
        </form>
      </Main>
    )
  }
}

Register.propTypes = {
  registerUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
}

const mapStateToProps = ({ auth, errors }) => ({
  auth,
  errors,
})

const mapDispatchToProps = dispatch => ({
  registerUser: (payload, history) => dispatch(registerUser(payload, history)),
})

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Register)
)
