/* eslint-disable no-underscore-dangle */
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import PostItem from '../PostItem/PostItem'

class PostList extends Component {
  render() {
    const { posts } = this.props

    return posts
      .sort((a, b) => new Date(b.from).getTime() - new Date(a.from).getTime())
      .map(post => <PostItem key={post._id} post={post} />)
  }
}

PostList.propTypes = {
  posts: PropTypes.array.isRequired,
}

export default PostList
