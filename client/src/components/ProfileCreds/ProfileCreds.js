import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Moment from 'react-moment'

class ProfileCreds extends Component {
  render() {
    const { experience, education } = this.props

    const expItems = experience
      .sort((a, b) => new Date(b.from).getTime() - new Date(a.from).getTime())
      .map(exp => (
        // eslint-disable-next-line no-underscore-dangle
        <div className="card experience-item" key={exp._id}>
          <header className="card-header">
            <p className="card-header-title">{exp.company}</p>
          </header>
          <div className="card-content">
            <div className="content">
              <p>
                <span className="has-text-weight-bold">From: </span>
                <span className="tag" style={{ margin: '0 0.5rem' }}>
                  <Moment format="YYYY/MM/DD">{exp.from}</Moment>
                </span>
                <span className="has-text-weight-bold"> To: </span>
                {exp.to ? (
                  <span className="tag" style={{ margin: '0 0.5rem' }}>
                    <Moment format="YYYY/MM/DD">{exp.from}</Moment>
                  </span>
                ) : (
                  <span className="tag" style={{ margin: '0 0.5rem' }}>
                    Now
                  </span>
                )}
              </p>
              <p>
                <span className="has-text-weight-bold">Position: </span>
                {exp.title}
              </p>
              {exp.location && (
                <p>
                  <span className="has-text-weight-bold">Location: </span>
                  {exp.location}
                </p>
              )}
              {exp.description && (
                <p>
                  <span className="has-text-weight-bold">Description: </span>
                  {exp.description}
                </p>
              )}
            </div>
          </div>
        </div>
      ))

    const eduItems = education
      .sort((a, b) => new Date(b.from).getTime() - new Date(a.from).getTime())
      .map(edu => (
        // eslint-disable-next-line no-underscore-dangle
        <div className="card education-item" key={edu._id}>
          <header className="card-header">
            <p className="card-header-title">{edu.school}</p>
          </header>
          <div className="card-content">
            <div className="content">
              <p>
                <span className="has-text-weight-bold">From: </span>
                <span className="tag" style={{ margin: '0 0.5rem' }}>
                  <Moment format="YYYY/MM/DD">{edu.from}</Moment>
                </span>
                <span className="has-text-weight-bold"> To: </span>
                {edu.to ? (
                  <span className="tag" style={{ margin: '0 0.5rem' }}>
                    <Moment format="YYYY/MM/DD">{edu.from}</Moment>
                  </span>
                ) : (
                  <span className="tag" style={{ margin: '0 0.5rem' }}>
                    Now
                  </span>
                )}
              </p>
              <p>
                <span className="has-text-weight-bold">Degree: </span>
                {edu.degree}
              </p>
              {edu.fieldOfStudy && (
                <p>
                  <span className="has-text-weight-bold">Field Of Study: </span>
                  {edu.fieldOfStudy}
                </p>
              )}
              {edu.description && (
                <p>
                  <span className="has-text-weight-bold">Description: </span>
                  {edu.description}
                </p>
              )}
            </div>
          </div>
        </div>
      ))

    const noItems = (
      <div className="card experience-item">
        <div className="card-content">
          <div className="content">
            <p>No records found...</p>
          </div>
        </div>
      </div>
    )

    return (
      <section className="container section-credentials">
        <div className="columns">
          <div className="column is-5-tablet is-offset-1-tablet experience-section">
            <div className="title is-4">
              <p className="has-text-centered">Experience</p>
            </div>
            {expItems.length > 0 ? expItems : noItems}
          </div>
          <div className="column is-5-tablet education-section">
            <div className="title is-4">
              <p className="has-text-centered">Education</p>
            </div>
            {eduItems.length > 0 ? eduItems : noItems}
          </div>
        </div>
      </section>
    )
  }
}

ProfileCreds.propTypes = {
  experience: PropTypes.array,
  education: PropTypes.array,
}

export default ProfileCreds
