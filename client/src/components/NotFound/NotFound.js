import React from 'react'

const NotFound = () => (
  <section className="hero not-found">
    <div className="hero-body">
      <div className="container has-text-centered">
        <div className="column is-6 is-offset-3">
          <h1 className="title is-size-1 has-text-weight-light">Sorry.</h1>
          <h2 className="subtitle">
            Looks like something went wrong on our hand.
          </h2>
        </div>
      </div>
    </div>
  </section>
)

export default NotFound
