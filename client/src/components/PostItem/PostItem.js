/* eslint-disable no-underscore-dangle */
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { deletePost, addLike, removeLike } from '../../actions/post'

class PostItem extends Component {
  constructor(props) {
    super(props)

    this.handleDeleteClick = this.handleDeleteClick.bind(this)
    this.handleLikeClick = this.handleLikeClick.bind(this)
    this.handleUnlikeClick = this.handleUnlikeClick.bind(this)
  }

  handleDeleteClick(id) {
    this.props.deletePost(id)
  }

  handleLikeClick(id) {
    this.props.addLike(id)
  }

  handleUnlikeClick(id) {
    this.props.removeLike(id)
  }

  hasLiked(likes) {
    const { auth } = this.props
    return likes.findIndex(like => like.user === auth.user.id) > -1
  }

  render() {
    const { post, auth, showActions } = this.props

    return (
      <div className="column is-8 is-offset-2">
        <div className="card">
          <div className="card-content">
            <article className="media">
              <figure className="media-left has-text-centered">
                <p className="image is-64x64">
                  <img src={post.avatar} className="is-rounded" alt="" />
                </p>
                <p className="title is-7">{post.name}</p>
              </figure>
              <div className="media-content">
                <div className="content">
                  <p>{post.text}</p>
                  {showActions && (
                    <div className="level">
                      <div className="level-left has-text-grey">
                        <span
                          className={classNames('icon post-like', {
                            'has-text-success': this.hasLiked(post.likes),
                          })}
                          onClick={() => this.handleLikeClick(post._id)}
                        >
                          <FontAwesomeIcon icon="grin" />
                        </span>
                        <span
                          className="icon post-like"
                          onClick={() => this.handleUnlikeClick(post._id)}
                        >
                          <FontAwesomeIcon icon="meh-blank" />
                        </span>
                        <span className="has-text-weight-bold">
                          {post.likes.length}
                        </span>
                      </div>
                      <div className="level-right buttons">
                        <Link
                          className="button is-small is-info"
                          to={`/post/${post._id}`}
                        >
                          <span>Comments</span>
                          <span className="icon is-small">
                            <FontAwesomeIcon icon="comments" />
                          </span>
                        </Link>
                      </div>
                    </div>
                  )}
                </div>
              </div>
              <div className="media-right">
                {post.user === auth.user.id && (
                  <button
                    onClick={() => this.handleDeleteClick(post._id)}
                    className="delete"
                  />
                )}
              </div>
            </article>
          </div>
        </div>
      </div>
    )
  }
}

PostItem.propTypes = {
  auth: PropTypes.object.isRequired,
  post: PropTypes.object.isRequired,
  deletePost: PropTypes.func.isRequired,
  addLike: PropTypes.func.isRequired,
  removeLike: PropTypes.func.isRequired,
  showActions: PropTypes.bool,
}

PostItem.defaultProps = {
  showActions: true,
}

const mapStateToProps = ({ auth }) => ({ auth })

const mapDispatchToProps = dispatch => ({
  deletePost: id => dispatch(deletePost(id)),
  addLike: id => dispatch(addLike(id)),
  removeLike: id => dispatch(removeLike(id)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PostItem)
