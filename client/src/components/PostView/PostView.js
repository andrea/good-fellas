import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { getPosts } from '../../actions/post'
import PostAdd from '../PostAdd/PostAdd'
import Spinner from '../common/Spinner'
import PostList from '../PostList/PostList'

class PostView extends Component {
  componentDidMount() {
    this.props.getPosts()
  }

  render() {
    const { posts, loading } = this.props.post

    return (
      <main className="posts">
        <div className="container">
          <section className="section">
            <PostAdd />
            {/* eslint-disable-next-line no-nested-ternary */}
            {posts === null && loading === true ? (
              <Spinner />
            ) : posts !== null && loading === false ? (
              <PostList posts={posts} />
            ) : null}
          </section>
        </div>
      </main>
    )
  }
}

PostView.propTypes = {
  post: PropTypes.object.isRequired,
  getPosts: PropTypes.func.isRequired,
}

const mapStateToProps = ({ post }) => ({ post })

const mapDispatchToProps = dispatch => ({
  getPosts: () => dispatch(getPosts()),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PostView)
