import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { isEmpty } from 'lodash'

import { getCurrentProfile } from '../../actions/profile'

// Components
import Main from '../common/Main'
import Spinner from '../common/Spinner'
import ProfileActions from '../ProfileActions/ProfileActions'
import ExperienceSummary from '../ExperienceSummary/ExperienceSummary'
import EducationSummary from '../EducationSummary/EducationSummary'

class Dashboard extends Component {
  constructor(props) {
    super(props)

    this.state = {
      zen: [
        'half measures are as bad as nothing at all',
        'non-blocking is better than blocking',
        'mind your words, they are important',
        'speak like a human',
        'approachable is better than simple',
        'favor focus over features',
        'anything added dilutes everything else',
        'responsive is better than fast',
        'practicality beats purity',
        'avoid administrative distraction',
        'design for failure',
        "it's not fully shipped until it's fast",
      ],
    }
  }

  componentDidMount() {
    this.props.getCurrentProfile()
  }

  render() {
    const { user } = this.props.auth
    const { profile, loading } = this.props.profile
    const { zen } = this.state

    let dashboardContent
    if (profile === null || loading) {
      dashboardContent = <Spinner />
    } else if (isEmpty(profile)) {
      // User is logged in but has no profile
      dashboardContent = (
        <div className="content">
          <p className="is-size-5">Welcome {user.name}</p>
          <p>You have not yet setup a profile, please add some info.</p>
          <Link to="/create-profile" className="button is-outlined is-info">
            Create Profile
          </Link>
        </div>
      )
    } else {
      // Check if logged in user has profile data
      dashboardContent = (
        <Fragment>
          <header className="hero notification is-small">
            <div className="hero-body">
              <h1 className="title is-4">Hello, {user.name}</h1>
              <h2 className="subtitle is-5">
                Remember: {zen[Math.floor(Math.random() * zen.length)]}.
              </h2>
            </div>
          </header>
          <ProfileActions />
          <ExperienceSummary experience={profile.experience} />
          <EducationSummary education={profile.education} />
        </Fragment>
      )
    }

    return (
      <Main
        title="Dashboard"
        subtitle="Here is your stuff. Guess I could write something a little more inspirational here..."
        mainClasses="dashboard"
        sectionClasses="column"
      >
        {dashboardContent}
      </Main>
    )
  }
}

Dashboard.propTypes = {
  auth: PropTypes.object.isRequired,
  getCurrentProfile: PropTypes.func.isRequired,
  profile: PropTypes.object.isRequired,
}

const mapStateToProps = ({ profile, auth }) => ({ profile, auth })

const mapDispatchToProps = dispatch => ({
  getCurrentProfile: () => dispatch(getCurrentProfile()),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Dashboard)
