import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

class ProfileItem extends Component {
  render() {
    const { profile } = this.props

    return (
      <div className="column is-half">
        <div className="card">
          <header className="card-header">
            <p className="card-header-title">{profile.user.name}</p>
            {profile.skills.length > 3 && (
              <span className="card-header-icon">
                <span className="icon has-text-info">
                  <FontAwesomeIcon icon="award" />
                </span>
              </span>
            )}
          </header>
          <div className="card-content">
            <article className="media">
              <figure className="media-left">
                <p className="image is-64x64">
                  <img
                    src={profile.user.avatar}
                    className="is-rounded"
                    alt=""
                  />
                </p>
              </figure>
              <div className="media-content">
                <div className="content">
                  <p>
                    <strong>{profile.status}</strong>
                    {profile.company && (
                      <Fragment>
                        <br />
                        <small>{profile.company}.</small>
                      </Fragment>
                    )}
                    {profile.location && (
                      <Fragment>
                        <small>{profile.location}.</small>
                      </Fragment>
                    )}
                  </p>
                  <div />
                </div>
              </div>
            </article>
            <p className="tags">
              {profile.skills.map(skill => (
                <span className="tag is-success" key={skill}>
                  <span className="icon">
                    <FontAwesomeIcon icon="check-circle" />
                  </span>
                  <span>{skill}</span>
                </span>
              ))}
            </p>
          </div>
          <footer className="card-footer">
            <span className="card-footer-item has-text-grey has-background-light">
              Add to favorites
            </span>
            <Link
              to={`/profile/${profile.handle}`}
              className="card-footer-item"
            >
              View Profile
            </Link>
          </footer>
        </div>
      </div>
    )
  }
}

ProfileItem.propTypes = {
  profile: PropTypes.object.isRequired,
}

export default ProfileItem
