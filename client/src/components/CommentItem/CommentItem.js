/* eslint-disable no-underscore-dangle */
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { deleteComment } from '../../actions/post'

class CommentItem extends Component {
  constructor(props) {
    super(props)

    this.handleDeleteClick = this.handleDeleteClick.bind(this)
  }

  handleDeleteClick(postId, commentId) {
    this.props.deleteComment(postId, commentId)
  }

  render() {
    const { comment, postId, auth } = this.props

    return (
      <div className="column is-8 is-offset-2">
        <div className="card">
          <div className="card-content">
            <article className="media">
              <figure className="media-left has-text-centered">
                <p className="image is-64x64">
                  <img src={comment.avatar} className="is-rounded" alt="" />
                </p>
                <p className="title is-7">{comment.name}</p>
              </figure>
              <div className="media-content">
                <div className="content">
                  <p>{comment.text}</p>
                </div>
              </div>
              <div className="media-right">
                {comment.user === auth.user.id && (
                  <button
                    onClick={() => this.handleDeleteClick(postId, comment._id)}
                    className="delete"
                  />
                )}
              </div>
            </article>
          </div>
        </div>
      </div>
    )
  }
}

CommentItem.propTypes = {
  deleteComment: PropTypes.func.isRequired,
  comment: PropTypes.object.isRequired,
  postId: PropTypes.string.isRequired,
  auth: PropTypes.object.isRequired,
}

const mapStateToProps = ({ auth }) => ({ auth })

const mapDispatchToProps = dispatch => ({
  deleteComment: (postId, commentId) =>
    dispatch(deleteComment(postId, commentId)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CommentItem)
