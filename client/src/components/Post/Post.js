/* eslint-disable no-underscore-dangle */
import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'

import { connect } from 'react-redux'
import _ from 'lodash'

import { getPost } from '../../actions/post'
import PostItem from '../PostItem/PostItem'
import Spinner from '../common/Spinner'
import CommentForm from '../CommentAdd/CommentAdd'
import CommentList from '../CommentList/CommentList'

class Post extends Component {
  componentDidMount() {
    this.props.getPost(this.props.match.params.id)
  }

  render() {
    const { post, loading } = this.props.post

    return (
      <main className="post">
        <div className="container">
          <section className="section">
            {post === null || loading || _.isEmpty(post) ? (
              <Spinner />
            ) : (
              <Fragment>
                <PostItem post={post} showActions={false} />
                <CommentForm postId={post._id} />
                <CommentList comments={post.comments} postId={post._id} />
              </Fragment>
            )}
          </section>
        </div>
      </main>
    )
  }
}

Post.propTypes = {
  getPost: PropTypes.func.isRequired,
  post: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  loading: PropTypes.bool,
}

const mapStateToProps = ({ post }) => ({ post })

const mapDispatchToProps = dispatch => ({
  getPost: id => dispatch(getPost(id)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Post)
