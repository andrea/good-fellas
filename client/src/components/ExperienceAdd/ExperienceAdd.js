import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link, withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import _ from 'lodash'

import { addExperience } from '../../actions/profile'

import InputField from '../common/InputField'
import TextAreaField from '../common/TextAreaField'
import Main from '../common/Main'

class ExperienceAdd extends Component {
  constructor(props) {
    super(props)

    this.state = {
      experience: {
        company: '',
        title: '',
        location: '',
        from: '',
        to: '',
        isCurrent: false,
        description: '',
      },
      errors: {},
    }

    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.handleCheckbox = this.handleCheckbox.bind(this)
  }

  static getDerivedStateFromProps(nextProps) {
    if (!_.isEmpty(nextProps.errors)) {
      return {
        errors: nextProps.errors,
      }
    }

    // Return null to indicate no change to state.
    return null
  }

  handleChange(e) {
    e.persist()
    this.setState(s => ({
      experience: { ...s.experience, [e.target.name]: e.target.value },
    }))
  }

  handleCheckbox() {
    this.setState(s => ({
      experience: {
        ...s.experience,
        isCurrent: !s.experience.isCurrent,
      },
    }))
  }

  handleSubmit(e) {
    e.preventDefault()
    this.props.addExperience(this.state.experience, this.props.history)
  }

  render() {
    const { errors, experience } = this.state

    return (
      <Main
        mainClasses="add-experience"
        sectionClasses="column is-8-tablet is-offset-2-tablet is-12-mobile"
        title="Add Experience"
        headerClasses="has-text-centered"
        subtitle="Add any job or position that you have had in the past or current."
      >
        <form noValidate onSubmit={this.handleSubmit}>
          <InputField
            id="company"
            name="company"
            label="Company"
            placeholder="Company"
            value={experience.company}
            onChange={this.handleChange}
            error={errors.company}
            info="Could be your own company or one you work for."
            icon="industry"
            isFullWidth
            isRequired
            isInputLarge
          />

          <InputField
            id="title"
            name="title"
            label="Job Title"
            placeholder="Job Title"
            value={experience.title}
            onChange={this.handleChange}
            error={errors.title}
            info="Your position."
            icon="user-tie"
            isFullWidth
            isRequired
            isInputLarge
          />

          <InputField
            id="location"
            name="location"
            label="Location"
            placeholder="Location"
            value={experience.location}
            onChange={this.handleChange}
            error={errors.location}
            info="City or city &amp; state (eg. Toronto, ON)."
            icon="globe-americas"
            isFullWidth
            isInputLarge
          />

          <InputField
            id="from"
            name="from"
            label="From Date"
            type="date"
            placeholder="1970-01-01"
            value={experience.from}
            onChange={this.handleChange}
            error={errors.from}
            info="Starting from..."
            icon="calendar-alt"
            isFullWidth
            isRequired
            isInputLarge
          />

          <InputField
            id="to"
            name="to"
            label="To Date"
            type="date"
            value={experience.to}
            onChange={this.handleChange}
            error={errors.to}
            info="Util..."
            icon="calendar-check"
            isFullWidth
            disabled={experience.isCurrent}
            isInputLarge
          />

          <div className="field">
            <div className="control level">
              <label className="checkbox level-left" htmlFor="current">
                <input
                  id="current"
                  type="checkbox"
                  name="current"
                  value={experience.isCurrent}
                  checked={experience.isCurrent}
                  onChange={this.handleCheckbox}
                />
                <span className="level-left" style={{ marginLeft: '0.5rem' }}>
                  Current Job
                </span>
              </label>
            </div>
          </div>

          <TextAreaField
            placeholder="Job description..."
            name="description"
            id="description"
            label="Job Description"
            isLabelVisible={false}
            value={experience.description}
            onChange={this.handleChange}
            error={errors.description}
            info="Tell us about this position."
            isInputLarge
          />

          <div className="section buttons is-centered">
            <button className="button is-link is-large" type="submit">
              Add Experience
            </button>
            <Link to="/dashboard" className="button is-large">
              Cancel
            </Link>
          </div>
        </form>
      </Main>
    )
  }
}

ExperienceAdd.propTypes = {
  profile: PropTypes.object,
  errors: PropTypes.object,
  history: PropTypes.object,
  addExperience: PropTypes.func.isRequired,
}

const mapStateToProps = () => ({ profile, errors }) => ({ profile, errors })

const mapDispatchToProps = dispatch => ({
  addExperience: (expData, history) =>
    dispatch(addExperience(expData, history)),
})

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(ExperienceAdd)
)
