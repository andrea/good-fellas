import React from 'react'

const Footer = () => (
  <footer className="footer">
    <p className="content has-text-centered">
      Copyright &copy; {new Date().getFullYear()} Profilr. All rights reserved.
    </p>
  </footer>
)

export default Footer
