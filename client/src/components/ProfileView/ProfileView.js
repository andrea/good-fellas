import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'

import { getProfileByHandle } from '../../actions/profile'

import ProfileHeader from '../ProfileHeader/ProfileHeader'
import ProfileAbout from '../ProfileAbout/ProfileAbout'
import ProfileCreds from '../ProfileCreds/ProfileCreds'
import ProfileGithub from '../ProfileGithub/ProfileGithub'
import Spinner from '../common/Spinner'

class ProfileView extends Component {
  componentDidMount() {
    if (this.props.match.params.handle) {
      this.props.getProfileByHandle(this.props.match.params.handle)
    }
  }

  render() {
    const { profile, loading } = this.props.profile
    if (profile === null && loading) {
      return (
        <main className="profile-view">
          <Spinner />
        </main>
      )
    }

    if (profile === null && loading === false) {
      return <Redirect to="/not-found" />
    }

    return (
      <main className="profile-view has-background-white-bis">
        <ProfileHeader
          user={profile.user}
          status={profile.status}
          company={profile.company}
          location={profile.location}
          websiteURL={profile.websiteURL}
          social={profile.social}
        />
        <ProfileAbout
          name={profile.user.name}
          bio={profile.bio}
          skills={profile.skills}
        />
        <ProfileCreds
          education={profile.education}
          experience={profile.experience}
        />
        {profile.githubUserName && (
          <ProfileGithub username={profile.githubUserName} />
        )}
      </main>
    )
  }
}

ProfileView.propTypes = {
  profile: PropTypes.object.isRequired,
  match: PropTypes.object,
  getProfileByHandle: PropTypes.func.isRequired,
}

const mapStateToProps = ({ profile }) => ({ profile })

const mapDispatchToProps = dispatch => ({
  getProfileByHandle: handle => dispatch(getProfileByHandle(handle)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProfileView)
