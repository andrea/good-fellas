import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

class ProfileHeader extends Component {
  render() {
    const { user, status, company, location, websiteURL, social } = this.props

    return (
      <section className="hero is-warning is-medium is-bold">
        <div className="hero-body container has-text-centered">
          <div className="is-flex avatar">
            <figure className="image is-128x128">
              <img className="is-rounded" src={user.avatar} alt="" />
            </figure>
          </div>
          <h1 className="title">{user.name}</h1>
          <p className="subtitle">
            {status}
            {company && (
              <Fragment>
                <span> at</span> {company}
              </Fragment>
            )}
          </p>
          <p>{location && location}</p>
          <div className="profile-social-media-icons">
            {websiteURL && (
              <a
                href={`https://${websiteURL}`}
                target="_blank"
                rel="noopener noreferrer"
              >
                <span className="icon fa-2x">
                  <FontAwesomeIcon icon="compass" />
                </span>
              </a>
            )}
            {social.twitterURL && (
              <a
                href={`https://${social.twitterURL}`}
                target="_blank"
                rel="noopener noreferrer"
              >
                <span className="icon fa-2x">
                  <FontAwesomeIcon icon={['fab', 'twitter']} />
                </span>
              </a>
            )}
            {social.facebookURL && (
              <a
                href={`https://${social.facebookURL}`}
                target="_blank"
                rel="noopener noreferrer"
              >
                <span className="icon fa-2x">
                  <FontAwesomeIcon icon={['fab', 'facebook']} />
                </span>
              </a>
            )}
            {social.linkedinURL && (
              <a
                href={`https://${social.linkedinURL}`}
                target="_blank"
                rel="noopener noreferrer"
              >
                <span className="icon fa-2x">
                  <FontAwesomeIcon icon={['fab', 'linkedin']} />
                </span>
              </a>
            )}
            {social.youtubeURL && (
              <a
                href={`https://${social.youtubeURL}`}
                target="_blank"
                rel="noopener noreferrer"
              >
                <span className="icon fa-2x">
                  <FontAwesomeIcon icon={['fab', 'youtube']} />
                </span>
              </a>
            )}
            {social.instagramURL && (
              <a
                href={`https://${social.instagramURL}`}
                target="_blank"
                rel="noopener noreferrer"
              >
                <span className="icon fa-2x">
                  <FontAwesomeIcon icon={['fab', 'instagram']} />
                </span>
              </a>
            )}
          </div>
        </div>
      </section>
    )
  }
}

ProfileHeader.propTypes = {
  user: PropTypes.object.isRequired,
  status: PropTypes.string.isRequired,
  company: PropTypes.string,
  location: PropTypes.string,
  websiteURL: PropTypes.string,
  social: PropTypes.object.isRequired,
}

export default ProfileHeader
