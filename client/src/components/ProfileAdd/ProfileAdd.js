/* eslint-disable react/no-unescaped-entities */
import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { Link, withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import _ from 'lodash'

import { createProfile } from '../../actions/profile'

// Components
import Main from '../common/Main'
import InputField from '../common/InputField'
import TextAreaField from '../common/TextAreaField'
import SelectList from '../common/SelectList'

class ProfileAdd extends Component {
  constructor(props) {
    super(props)

    this.state = {
      displaySocialInputs: false,
      user: {
        handle: '',
        company: '',
        websiteURL: '',
        location: '',
        status: '',
        skills: '',
        githubUserName: '',
        bio: '',
        twitterURL: '',
        facebookURL: '',
        linkedinURL: '',
        youtubeURL: '',
        instagramURL: '',
      },
      errors: {},
    }

    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleSocialMediaLinks = this.handleSocialMediaLinks.bind(this)
  }

  static getDerivedStateFromProps(nextProps) {
    if (!_.isEmpty(nextProps.errors)) {
      return {
        errors: nextProps.errors,
      }
    }

    // Return null to indicate no change to state.
    return null
  }

  handleChange(e) {
    e.persist()
    this.setState(s => ({
      user: { ...s.user, [e.target.name]: e.target.value },
    }))
  }

  handleSocialMediaLinks() {
    this.setState(s => ({ displaySocialInputs: !s.displaySocialInputs }))
  }

  handleSubmit(e) {
    e.preventDefault()
    this.props.createProfile(this.state.user, this.props.history)
  }

  // eslint-disable-next-line class-methods-use-this
  render() {
    const { errors, user, displaySocialInputs } = this.state

    // Select options for status
    const options = [
      { label: 'Select Professional Status', value: '' },
      { label: 'Developer', value: 'Developer' },
      { label: 'Junior Developer', value: 'Junior Developer' },
      { label: 'Senior Developer', value: 'Senior Developer' },
      { label: 'Manager', value: 'Manager' },
      { label: 'Student or Learning', value: 'Student or Learning' },
      { label: 'Instructor or teacher', value: 'Instructor or teacher' },
      { label: 'Intern', value: 'Intern' },
      { label: 'Other', value: 'Other' },
    ]

    let socialInputs

    if (displaySocialInputs) {
      socialInputs = (
        <Fragment>
          <InputField
            id="twitterURL"
            name="twitterURL"
            label="Twitter handle"
            placeholder="twitter.com/me"
            value={user.twitterURL}
            onChange={this.handleChange}
            error={errors.twitterURL}
            icon={['fab', 'twitter']}
            isFullWidth
            isInputLarge
          />
          <InputField
            id="facebookURL"
            name="facebookURL"
            label="Facebook profile"
            placeholder="facebook.com/me"
            value={user.facebookURL}
            onChange={this.handleChange}
            error={errors.facebookURL}
            icon={['fab', 'facebook']}
            isFullWidth
            isInputLarge
          />
          <InputField
            id="linkedinURL"
            name="linkedinURL"
            label="Linkedin Page"
            placeholder="linkedin.com/me"
            value={user.linkedinURL}
            onChange={this.handleChange}
            error={errors.linkedinURL}
            icon={['fab', 'linkedin']}
            isFullWidth
            isInputLarge
          />
          <InputField
            id="youtubeURL"
            name="youtubeURL"
            label="YouTube Page"
            placeholder="youtube.com/me"
            value={user.youtubeURL}
            onChange={this.handleChange}
            error={errors.youtubeURL}
            icon={['fab', 'youtube']}
            isFullWidth
            isInputLarge
          />
          <InputField
            id="instagramURL"
            name="instagramURL"
            label="Instagram Profile"
            placeholder="instagram.com/me"
            value={user.instagramURL}
            onChange={this.handleChange}
            error={errors.instagramURL}
            icon={['fab', 'instagram']}
            isFullWidth
            isInputLarge
          />
        </Fragment>
      )
    }

    return (
      <Main
        title="Create your Profile"
        subtitle="Let's get some information to make your profile stand out."
        mainClasses="create-profile"
        headerClasses="has-text-centered"
        sectionClasses="column is-8-tablet is-offset-2-tablet is-12-mobile"
      >
        <form noValidate onSubmit={this.handleSubmit}>
          <InputField
            id="handle"
            name="handle"
            label="Custom address"
            placeholder="Profile Handle"
            value={user.handle}
            onChange={this.handleChange}
            error={errors.handle}
            info="A unique handle for your profile URL."
            icon="user-circle"
            isFullWidth
            isRequired
            isInputLarge
          />
          <SelectList
            id="status"
            name="status"
            label="Status"
            placeholder="Occupation"
            value={user.status}
            onChange={this.handleChange}
            error={errors.status}
            options={options}
            info="Give us an idea of where you are at in your career."
            icon="briefcase"
            isFullWidth
            isRequired
          />
          <InputField
            id="company"
            name="company"
            label="Company"
            placeholder="Company"
            value={user.company}
            onChange={this.handleChange}
            error={errors.company}
            info="Could be your own company or one you work for."
            icon="industry"
            isFullWidth
            isInputLarge
          />
          <InputField
            id="websiteURL"
            name="websiteURL"
            label="Website URL"
            placeholder="domain.com"
            value={user.websiteURL}
            onChange={this.handleChange}
            error={errors.websiteURL}
            info="Could be your own website or a company one."
            icon="compass"
            isFullWidth
            isInputLarge
          />
          <InputField
            id="location"
            name="location"
            label="Location"
            placeholder="Location"
            value={user.location}
            onChange={this.handleChange}
            error={errors.location}
            info="City or city &amp; state (eg. Toronto, ON)."
            icon="globe-americas"
            isFullWidth
            isInputLarge
          />
          <InputField
            id="skills"
            name="skills"
            label="Skills"
            placeholder="Skills"
            value={user.skills}
            onChange={this.handleChange}
            error={errors.skills}
            info="Please use comma separated values (eg. HTML, CSS, JavaScript)."
            icon="check-circle"
            isFullWidth
            isRequired
            isInputLarge
          />
          <InputField
            id="githubUserName"
            name="githubUserName"
            label="GitHub Username"
            placeholder="GitHub username"
            value={user.githubUserName}
            onChange={this.handleChange}
            error={errors.githubUserName}
            info="If you want your latest repos and GitHub link, include your username."
            icon={['fab', 'github']}
            isFullWidth
            isInputLarge
          />
          <TextAreaField
            placeholder="Short personal bio..."
            name="bio"
            id="bio"
            label="About"
            isLabelVisible
            value={user.bio}
            onChange={this.handleChange}
            error={errors.bio}
            info="Tell us a little about yourself."
            isInputLarge
          />
          <hr />
          <button
            type="button"
            onClick={this.handleSocialMediaLinks}
            className="button is-medium is-fullwidth is-light has-text-grey"
          >
            Additional Social Network Links
          </button>
          <div className="displaySocialInputs">{socialInputs}</div>
          <div className="section buttons is-centered">
            <button className="button is-link is-large" type="submit">
              Create Profile
            </button>
            <Link to="/dashboard" className="button is-large">
              Cancel
            </Link>
          </div>
        </form>
      </Main>
    )
  }
}

ProfileAdd.propTypes = {
  profile: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  createProfile: PropTypes.func.isRequired,
}

const mapStateToProps = ({ profile, errors }) => ({
  profile,
  errors,
})

const mapDispatchToProps = dispatch => ({
  createProfile: (payload, history) =>
    dispatch(createProfile(payload, history)),
})

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(ProfileAdd)
)
