import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

class ProfileAbout extends Component {
  render() {
    const { name, bio, skills } = this.props

    return (
      <section className="section container section-about">
        <div className="columns features">
          <div className="column is-10-tablet is-offset-1-tablet">
            {bio && (
              <div className="box content">
                <h2 className="title has-text-centered">
                  {name && name.split(' ')[0]}
                  &apos;s Bio
                </h2>
                <p>{bio}</p>
              </div>
            )}
            <div className="box is-rounded skill-section">
              <div className="content">
                <h2 className="title has-text-centered">Skill Set</h2>
                <p className="tags">
                  {skills.map(skill => (
                    <span className="tag is-success is-medium" key={skill}>
                      <span className="icon">
                        <FontAwesomeIcon icon="check-circle" />
                      </span>
                      <span>{skill}</span>
                    </span>
                  ))}
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>
    )
  }
}

ProfileAbout.propTypes = {
  name: PropTypes.string.isRequired,
  bio: PropTypes.string,
  skills: PropTypes.array.isRequired,
}

export default ProfileAbout
