import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import classNames from 'classnames'

import { logoutUser } from '../../actions/auth'
import { clearCurrentProfile } from '../../actions/profile'

class Navbar extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isActive: false,
    }

    this.handleHamburger = this.handleHamburger.bind(this)
    this.handleLogout = this.handleLogout.bind(this)
  }

  handleHamburger() {
    this.setState(({ isActive }) => ({ isActive: !isActive }))
  }

  handleLogout(e) {
    e.preventDefault()
    this.props.clearCurrentProfile()
    this.props.logoutUser()
  }

  render() {
    const { isActive } = this.state
    const { isAuthenticated, user } = this.props.auth

    const authLinks = (
      <Fragment>
        <Link className="navbar-item" to="/wall">
          Wall
        </Link>
        <Link className="navbar-item" to="/dashboard">
          Dashboard
        </Link>
        <a type="button" onClick={this.handleLogout} className="navbar-item">
          <figure className="image is-24x24 is-inline-block level is-hidden-touch">
            <img
              className="is-rounded"
              src={user.avatar}
              alt={user.name}
              title="You must have Gravatar connected to your email to display a custom picture"
            />
          </figure>
          Logout
        </a>
      </Fragment>
    )

    const guestLinks = (
      <Fragment>
        <Link className="navbar-item" to="/register">
          Sign Up
        </Link>
        <Link className="navbar-item" to="/login">
          Login
        </Link>
      </Fragment>
    )

    return (
      <header className="navbar is-dark">
        <nav className="container">
          <div className="navbar-brand">
            <Link className="navbar-item" to="/">
              Home
            </Link>
            <div
              className={classNames('navbar-burger', 'burger', {
                'is-active': isActive,
              })}
              data-target="navbar-mobile"
              onClick={this.handleHamburger}
            >
              <span />
              <span />
              <span />
            </div>
          </div>

          <div
            id="navbar-mobile"
            className={classNames('navbar-menu', { 'is-active': isActive })}
          >
            <div className="navbar-start">
              <Link className="navbar-item" to="/profiles">
                Profiles
              </Link>
            </div>

            <div className="navbar-end">
              {isAuthenticated ? authLinks : guestLinks}
            </div>
          </div>
        </nav>
      </header>
    )
  }
}

Navbar.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  clearCurrentProfile: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
}

const mapStateToProps = ({ auth }) => ({ auth })

const mapDispatchToProps = dispatch => ({
  logoutUser: () => dispatch(logoutUser()),
  clearCurrentProfile: () => dispatch(clearCurrentProfile()),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Navbar)
