import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import _ from 'lodash'

import { addPost } from '../../actions/post'

import TextAreaField from '../common/TextAreaField'

class PostAdd extends Component {
  constructor(props) {
    super(props)

    this.state = {
      text: '',
      errors: {},
    }

    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleSubmitOnKeyDown = this.handleSubmitOnKeyDown.bind(this)
  }

  static getDerivedStateFromProps(nextProps) {
    if (!_.isEmpty(nextProps.errors)) {
      return {
        errors: nextProps.errors,
      }
    }

    // Return null to indicate no change to state.
    return null
  }

  componentDidMount() {
    document.addEventListener('keydown', this.handleSubmitOnKeyDown)
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.handleSubmitOnKeyDown)
  }

  handleSubmitOnKeyDown(event) {
    if (event.keyCode === 13 && event.metaKey) {
      this.handleSubmit(event)
    }
  }

  handleChange(e) {
    e.persist()
    this.setState(() => ({ [e.target.name]: e.target.value }))
  }

  handleSubmit(e) {
    e.preventDefault()
    const { user } = this.props.auth

    const newPost = {
      text: this.state.text,
      name: user.name,
      avatar: user.avatar,
    }

    this.props.addPost(newPost)
    this.setState(() => ({ text: '' }))
  }

  render() {
    return (
      <div className="column is-8 is-offset-2">
        <article className="message is-info">
          <header className="message-header">
            <p>Say Something nice...</p>
          </header>
          <div className="message-body">
            <form noValidate onSubmit={this.handleSubmit} ref={this.formRef}>
              <TextAreaField
                placeholder="Message..."
                name="text"
                id="text"
                label="Post"
                isLabelHidden
                value={this.state.text}
                onChange={this.handleChange}
                error={this.state.errors.text}
                info="Create a post"
              />
              <div className="buttons is-right">
                <button className="button" type="submit">
                  Post message
                </button>
              </div>
            </form>
          </div>
        </article>
      </div>
    )
  }
}

PostAdd.propTypes = {
  addPost: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
}

const mapStateToProps = ({ auth, errors }) => ({ auth, errors })

const mapDispatchToProps = dispatch => ({
  addPost: postData => dispatch(addPost(postData)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PostAdd)
