import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { loginUser } from '../../actions/auth'

// Components
import Main from '../common/Main'
import InputField from '../common/InputField'

class Login extends Component {
  constructor(props) {
    super(props)
    this.state = {
      user: {
        email: '',
        password: '',
      },
      errors: {},
    }

    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.errors !== prevState.errors) {
      return { errors: nextProps.errors }
    }

    if (nextProps.auth.isAuthenticated) {
      nextProps.history.push('/dashboard')
    }

    // Return null to indicate no change to state.
    return null
  }

  componentDidMount() {
    if (this.props.auth.isAuthenticated) {
      this.props.history.push('/dashboard')
    }
  }

  handleChange(e) {
    e.persist()
    this.setState(s => ({
      user: { ...s.user, [e.target.name]: e.target.value },
    }))
  }

  handleSubmit(e) {
    e.preventDefault()
    this.props.loginUser(this.state.user)
  }

  render() {
    const { errors } = this.state

    return (
      <Main
        mainClasses="login"
        headerClasses="has-text-centered"
        title="Login"
        subtitle="Sign in to your Profilr account."
        sectionClasses="column is-8-tablet is-offset-2-tablet is-12-mobile"
      >
        <form noValidate onSubmit={this.handleSubmit}>
          <InputField
            id="email"
            name="email"
            type="email"
            label="Email"
            placeholder="hello@domain.com"
            value={this.state.user.email}
            onChange={this.handleChange}
            icon="at"
            error={errors.email}
            isRequired
            isFullWidth
            isInputLarge
          />
          <InputField
            id="password"
            name="password"
            type="password"
            autocomplete="current-password"
            label="Password"
            placeholder="password"
            value={this.state.user.password}
            onChange={this.handleChange}
            icon="lock"
            error={errors.password}
            isRequired
            isFullWidth
            isInputLarge
          />
          <div className="field">
            <div className="control">
              <label className="label is-invisible">Access your Account</label>
              <button
                type="submit"
                className="button is-link is-large is-fullwidth"
              >
                Submit
              </button>
            </div>
          </div>
        </form>
      </Main>
    )
  }
}

Login.propTypes = {
  loginUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
}

const mapStateToProps = ({ auth, errors }) => ({ auth, errors })

const mapDispatchToProps = dispatch => ({
  loginUser: payload => dispatch(loginUser(payload)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login)
