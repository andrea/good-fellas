import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

class ProfileGithub extends Component {
  constructor(props) {
    super(props)

    this.state = {
      // https://github.com/facebook/create-react-app/tree/master/packages/react-scripts/template#adding-custom-environment-variables
      clientId: process.env.REACT_APP_GITHUB_CLIENT_ID,
      clientSecret: process.env.REACT_APP_GITHUB_CLIENT_SECRET,
      repoCount: 5,
      sort: 'created: asc',
      repos: [],
    }
  }

  componentDidMount() {
    const { username } = this.props
    const { repoCount, sort, clientId, clientSecret } = this.state
    const uri = encodeURI(
      `https://api.github.com/users/${username}/repos?per_page=${repoCount}&sort=${sort}&client_id=${clientId}&client_secret=${clientSecret}`
    )
    fetch(uri)
      .then(res => res.json())
      .then(data => {
        if (Array.isArray(data)) {
          this.setState(() => ({ repos: data }))
        }
      })
      // eslint-disable-next-line no-console
      .catch(err => console.log(err))
  }

  render() {
    const { repos } = this.state
    const repoItems = repos.map(repo => (
      <div className="card repo-item" key={repo.id}>
        <div className="card-content">
          <div className="content">
            <article className="repo">
              <h4>
                <a
                  href={repo.html_url}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  {repo.name}
                </a>
              </h4>
              <div className="media">
                <div className="media-content">
                  <div className="content">
                    <p>{repo.description}</p>
                  </div>
                </div>
                <div className="media-right tags is-hidden-mobile">
                  <span className="tag">
                    <span className="icon">
                      <FontAwesomeIcon icon="star" />
                    </span>
                    <span>Stars: {repo.stargazers_count}</span>
                  </span>
                  <span className="tag">
                    <span className="icon">
                      <FontAwesomeIcon icon="eye" />
                    </span>
                    <span>Watchers: {repo.watchers_count}</span>
                  </span>
                  <span className="tag">
                    <span className="icon">
                      <FontAwesomeIcon icon="code-branch" />
                    </span>
                    <span>Forks: {repo.forks_count}</span>
                  </span>
                </div>
              </div>
            </article>
          </div>
        </div>
      </div>
    ))

    const noItems = (
      <div className="card experience-item">
        <div className="card-content">
          <div className="content">
            <p>No records found...</p>
          </div>
        </div>
      </div>
    )

    return (
      <section className="container section-github">
        <div className="columns">
          <div className="column is-10-tablet is-offset-1-tablet">
            <div className="title is-4">
              <p className="has-text-centered">Latest Github repos</p>
            </div>
            {repoItems.length > 0 ? repoItems : noItems}
          </div>
        </div>
      </section>
    )
  }
}

ProfileGithub.propTypes = {
  username: PropTypes.string.isRequired,
}

export default ProfileGithub
