import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'

const TextAreaField = ({
  id,
  name,
  placeholder,
  value,
  label,
  error,
  isFullWidth,
  onChange,
  isLabelVisible,
  isLabelHidden,
  isInputLarge,
}) => (
  <div className="field">
    <label
      className={classNames('label', {
        'is-invisible': !isLabelVisible,
        'is-hidden': isLabelHidden,
      })}
      htmlFor={id}
    >
      {label}
    </label>
    <div className="control">
      <textarea
        id={id}
        name={name}
        className={classNames('textarea', {
          'is-large': isInputLarge,
          'is-danger': error,
          'is-fullwidth': isFullWidth,
        })}
        placeholder={placeholder}
        value={value}
        onChange={onChange}
      />
    </div>
    {error && <p className="help is-danger">{error}</p>}
  </div>
)

TextAreaField.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  value: PropTypes.string.isRequired,
  label: PropTypes.string,
  isLabelVisible: PropTypes.bool,
  isLabelHidden: PropTypes.bool,
  isInputLarge: PropTypes.bool,
  isFullWidth: PropTypes.bool,
  error: PropTypes.string,
  onChange: PropTypes.func.isRequired,
}

TextAreaField.defaultProps = {
  label: '&nbsp;',
  isLabelVisible: true,
  isLabelHidden: false,
  isFullWidth: false,
  isInputLarge: false,
}

export default TextAreaField
