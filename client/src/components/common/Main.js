import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'

const Main = ({
  mainClasses,
  headerClasses,
  sectionClasses,
  title,
  subtitle,
  hasSubtitle,
  children,
}) => (
  <main className={`${mainClasses}`}>
    <header className={`${headerClasses} section container`}>
      <h1 className="title">{title}</h1>
      <p className={classNames('subtitle', { 'is-invisible': !hasSubtitle })}>
        {subtitle}
      </p>
    </header>
    <div className="container">
      <section className={`section ${sectionClasses}`}>{children}</section>
    </div>
  </main>
)

Main.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.object.isRequired,
    PropTypes.array.isRequired,
  ]),
  mainClasses: PropTypes.string.isRequired,
  sectionClasses: PropTypes.string,
  headerClasses: PropTypes.string,
  title: PropTypes.string,
  subtitle: PropTypes.string,
  hasSubtitle: PropTypes.bool,
}

Main.defaultProps = {
  title: 'Page Title',
  subtitle: 'Page Subtitle',
  hasSubtitle: true,
  headerClasses: '',
}

export default Main
