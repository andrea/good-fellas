import React from 'react'

const Spinner = () => (
  <div className="section columns is-centered">
    <div className="loader" title="Loading..." />
  </div>
)

export default Spinner
