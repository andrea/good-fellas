import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const SelectList = ({
  id,
  name,
  value,
  label,
  error,
  icon,
  isFullWidth,
  onChange,
  options,
  isLabelVisible,
  isRequired,
}) => {
  const selectOption = options.map(o => (
    <option value={o.value} key={o.label}>
      {o.label}
    </option>
  ))

  return (
    <div className="field">
      <label
        className={classNames('label', { 'is-invisible': !isLabelVisible })}
        htmlFor={id}
      >
        {label}
      </label>
      <p className="control has-icons-left">
        <span
          className={classNames(
            'select',
            'is-large',
            isFullWidth && 'is-fullwidth',
            isRequired && !error && 'is-primary'
          )}
        >
          <select id={id} name={name} value={value} onChange={onChange}>
            {selectOption}
          </select>
        </span>
        <span className="icon is-small is-left">
          <FontAwesomeIcon icon={icon} />
        </span>
      </p>
      {error && <p className="help is-danger">{error}</p>}
    </div>
  )
}

SelectList.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  label: PropTypes.string,
  isLabelVisible: PropTypes.bool,
  isRequired: PropTypes.bool,
  isFullWidth: PropTypes.bool,
  error: PropTypes.string,
  icon: PropTypes.oneOfType([
    PropTypes.string.isRequired,
    PropTypes.array.isRequired,
  ]),
  onChange: PropTypes.func.isRequired,
  options: PropTypes.array.isRequired,
}

SelectList.defaultProps = {
  label: '&nbsp;',
  isLabelVisible: true,
  isRequired: false,
  isFullWidth: false,
  icon: 'globe',
}

export default SelectList
