import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const InputField = ({
  id,
  name,
  placeholder,
  value,
  label,
  error,
  icon,
  type,
  onChange,
  disabled,
  isLabelVisible,
  isInputLarge,
  isFullWidth,
  isRequired,
}) => (
  <div className="field">
    <label
      className={classNames('label', { 'is-invisible': !isLabelVisible })}
      htmlFor={id}
    >
      {label}
    </label>
    <div className="control has-icons-left has-icons-right">
      <input
        id={id}
        name={name}
        className={classNames('input', {
          'is-large': isInputLarge,
          'is-danger': error,
          'is-fullwidth': isFullWidth,
        })}
        type={type}
        placeholder={placeholder}
        value={value}
        onChange={onChange}
        disabled={disabled}
      />
      <span className="icon is-small is-left">
        <FontAwesomeIcon icon={icon} />
      </span>

      <span
        className={classNames(
          'icon is-small',
          'is-right',
          !isRequired && 'is-hidden',
          isRequired && !error && 'has-text-primary',
          isRequired && error && 'has-text-danger'
        )}
      >
        <FontAwesomeIcon icon="exclamation-circle" />
      </span>
    </div>
    {error && <p className="help is-danger">{error}</p>}
  </div>
)

InputField.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  value: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  isLabelVisible: PropTypes.bool,
  isInputLarge: PropTypes.bool,
  isRequired: PropTypes.bool,
  isFullWidth: PropTypes.bool,
  error: PropTypes.string,
  icon: PropTypes.oneOfType([
    PropTypes.string.isRequired,
    PropTypes.array.isRequired,
  ]),
  type: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
}

InputField.defaultProps = {
  type: 'text',
  isLabelVisible: true,
  isRequired: false,
  isInputLarge: false,
  isFullWidth: false,
  icon: 'globe',
  disabled: false,
}

export default InputField
