import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import _ from 'lodash'

import { addComment } from '../../actions/post'

import TextAreaField from '../common/TextAreaField'

class CommentAdd extends Component {
  constructor(props) {
    super(props)

    this.state = {
      text: '',
      errors: {},
    }

    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleSubmitOnKeyDown = this.handleSubmitOnKeyDown.bind(this)
  }

  static getDerivedStateFromProps(nextProps) {
    if (!_.isEmpty(nextProps.errors)) {
      return {
        errors: nextProps.errors,
      }
    }

    // Return null to indicate no change to state.
    return null
  }

  componentDidMount() {
    document.addEventListener('keydown', this.handleSubmitOnKeyDown)
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.handleSubmitOnKeyDown)
  }

  handleSubmitOnKeyDown(event) {
    if (event.keyCode === 13 && event.metaKey) {
      this.handleSubmit(event)
    }
  }

  handleChange(e) {
    e.persist()
    this.setState(() => ({ [e.target.name]: e.target.value }))
  }

  handleSubmit(e) {
    e.preventDefault()
    const { user } = this.props.auth
    const { postId } = this.props

    const newComment = {
      text: this.state.text,
      name: user.name,
      avatar: user.avatar,
    }

    this.props.addComment(postId, newComment)
    this.setState(() => ({ text: '' }))
  }

  render() {
    return (
      <div className="column is-8 is-offset-2">
        <article className="message is-info">
          <header className="message-header">
            <p>Make a comment...</p>
          </header>
          <div className="message-body">
            <form noValidate onSubmit={this.handleSubmit} ref={this.formRef}>
              <TextAreaField
                placeholder="Reply to post..."
                name="text"
                id="text"
                label="Post"
                isLabelHidden
                value={this.state.text}
                onChange={this.handleChange}
                error={this.state.errors.text}
                info="Reply to post"
              />
              <div className="buttons is-right">
                <button className="button" type="submit">
                  Post message
                </button>
              </div>
            </form>
          </div>
        </article>
      </div>
    )
  }
}

CommentAdd.propTypes = {
  addComment: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
  postId: PropTypes.string.isRequired,
}

const mapStateToProps = ({ auth, errors }) => ({ auth, errors })

const mapDispatchToProps = dispatch => ({
  addComment: (postId, commentData) =>
    dispatch(addComment(postId, commentData)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CommentAdd)
