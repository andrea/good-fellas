import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Moment from 'react-moment'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { deleteExperience } from '../../actions/profile'

class ExperienceSummary extends Component {
  constructor(props) {
    super(props)

    this.handleDeleteExperience = this.handleDeleteExperience.bind(this)
  }

  handleDeleteExperience(id) {
    this.props.deleteExperience(id)
  }

  render() {
    const experience = this.props.experience
      .sort((a, b) => new Date(a.from).getTime() - new Date(b.from).getTime())
      .map(exp => (
        // eslint-disable-next-line no-underscore-dangle
        <tr key={exp._id}>
          <td>{exp.company}</td>
          <td>{exp.title}</td>
          <td>
            <Moment format="YYYY/MM/DD">{exp.from}</Moment>
          </td>
          <td>
            {exp.to === null ? (
              'Now'
            ) : (
              <Moment format="YYYY/MM/DD">{exp.to}</Moment>
            )}
          </td>
          <td>
            <a
              className="button is-danger is-outlined"
              // eslint-disable-next-line no-underscore-dangle
              onClick={() => this.handleDeleteExperience(exp._id)}
            >
              <span>Delete</span>
              <span className="icon is-small">
                <FontAwesomeIcon icon="trash-alt" />
              </span>
            </a>
          </td>
        </tr>
      ))

    return (
      <Fragment>
        <section className="experience-summary">
          <h3 className="title">Experience Credentials</h3>
          <table className="table is-striped is-hoverable is-fullwidth">
            <thead>
              <tr>
                <th>Company</th>
                <th>Title</th>
                <th>From</th>
                <th>To</th>
                <th>Action</th>
              </tr>
            </thead>
            <tfoot>
              <tr>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
              </tr>
            </tfoot>
            <tbody>{experience}</tbody>
          </table>
        </section>
      </Fragment>
    )
  }
}

ExperienceSummary.propTypes = {
  experience: PropTypes.array.isRequired,
  deleteExperience: PropTypes.func.isRequired,
}

const mapDispatchToProps = dispatch => ({
  deleteExperience: id => dispatch(deleteExperience(id)),
})

export default connect(
  null,
  mapDispatchToProps
)(ExperienceSummary)
