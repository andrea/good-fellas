import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Moment from 'react-moment'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { deleteEducation } from '../../actions/profile'

class EducationSummary extends Component {
  constructor(props) {
    super(props)

    this.handleDeleteEducation = this.handleDeleteEducation.bind(this)
  }

  handleDeleteEducation(id) {
    this.props.deleteEducation(id)
  }

  render() {
    const education = this.props.education
      .sort((a, b) => new Date(a.from).getTime() - new Date(b.from).getTime())
      .map(edu => (
        // eslint-disable-next-line no-underscore-dangle
        <tr key={edu._id}>
          <td>{edu.school}</td>
          <td>{edu.degree}</td>
          <td>
            <Moment format="YYYY/MM/DD">{edu.from}</Moment>
          </td>
          <td>
            {edu.to === null ? (
              'Now'
            ) : (
              <Moment format="YYYY/MM/DD">{edu.to}</Moment>
            )}
          </td>
          <td>
            <a
              className="button is-danger is-outlined"
              // eslint-disable-next-line no-underscore-dangle
              onClick={() => this.handleDeleteEducation(edu._id)}
            >
              <span>Delete</span>
              <span className="icon is-small">
                <FontAwesomeIcon icon="trash-alt" />
              </span>
            </a>
          </td>
        </tr>
      ))

    return (
      <Fragment>
        <section className="education-summary">
          <h3 className="title">Education Credentials</h3>
          <table className="table is-striped is-hoverable is-fullwidth">
            <thead>
              <tr>
                <th>School</th>
                <th>Degree</th>
                <th>From</th>
                <th>To</th>
                <th>Action</th>
              </tr>
            </thead>
            <tfoot>
              <tr>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
              </tr>
            </tfoot>
            <tbody>{education}</tbody>
          </table>
        </section>
      </Fragment>
    )
  }
}

EducationSummary.propTypes = {
  education: PropTypes.array.isRequired,
  deleteEducation: PropTypes.func.isRequired,
}

const mapDispatchToProps = dispatch => ({
  deleteEducation: id => dispatch(deleteEducation(id)),
})

export default connect(
  null,
  mapDispatchToProps
)(EducationSummary)
