import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { getProfiles } from '../../actions/profile'

// Components
import Main from '../common/Main'
import Spinner from '../common/Spinner'
import ProfileItem from '../ProfileItem/ProfileItem'

class ProfileList extends Component {
  componentDidMount() {
    this.props.getProfiles()
  }

  render() {
    const { profiles, loading } = this.props.profile
    let profileItems

    if (profiles === null && loading === true) {
      profileItems = <Spinner />
    } else if (profiles && profiles.length > 0) {
      profileItems = profiles.map(profile => (
        // eslint-disable-next-line no-underscore-dangle
        <ProfileItem key={profile._id} profile={profile} />
      ))
    } else {
      profileItems = (
        <div className="column">
          <h4>No Profiles found.</h4>
        </div>
      )
    }

    return (
      <Main
        title="All Profiles"
        subtitle="Browse and connect with whomever you like. Some serious lack of fantasy here..."
        mainClasses="profile-list"
        sectionClasses="column"
      >
        <div className="columns is-multiline">{profileItems}</div>
      </Main>
    )
  }
}

ProfileList.propTypes = {
  getProfiles: PropTypes.func.isRequired,
  profile: PropTypes.object.isRequired,
}

const mapStateToProps = ({ profile }) => ({ profile })

const mapDispatchToProps = dispatch => ({
  getProfiles: () => dispatch(getProfiles()),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProfileList)
