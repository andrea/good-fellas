import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { Provider } from 'react-redux'
import jwtDecode from 'jwt-decode'

import store from '../../store'
import setAuthToken from '../../utils/setAuthToken'
import { logoutUser, setCurrentUser } from '../../actions/auth'
import { clearCurrentProfile } from '../../actions/profile'

// FontAwesome Icon Library
import '../../faLib'

// Components
import Dashboard from '../Dashboard/Dashboard'
import Footer from '../Footer/Footer'
import Landing from '../Landing/Landing'
import Login from '../Login/Login'
import Navbar from '../Navbar/Navbar'
import PrivateRoute from '../common/PrivateRoute'
import Register from '../Register/Register'
import ProfileAdd from '../ProfileAdd/ProfileAdd'
import ProfileEdit from '../ProfileEdit/ProfileEdit'
import ExperienceAdd from '../ExperienceAdd/ExperienceAdd'
import EducationAdd from '../EducationAdd/EducationAdd'
import ProfileList from '../ProfileList/ProfileList'
import ProfileView from '../ProfileView/ProfileView'
import PostView from '../PostView/PostView'
import Post from '../Post/Post'
import NotFound from '../NotFound/NotFound'

// Check for Authentication Token
if (localStorage.jwtToken) {
  // Set Authorization token header
  setAuthToken(localStorage.jwtToken)
  // Decode token and get user info and expiration
  const userData = jwtDecode(localStorage.jwtToken)
  // Set user and isAuthenticated
  store.dispatch(setCurrentUser(userData))

  // Check for expired token
  const currentTime = Date.now() / 1000
  if (userData.exp < currentTime) {
    // Logout user
    store.dispatch(logoutUser())
    // Clear current profile
    store.dispatch(clearCurrentProfile())
    // Redirect to login
    window.location.href = '/login'
  }
}

const App = () => (
  <Provider store={store}>
    <Router>
      <div className="app">
        <Navbar />
        <Switch>
          <Route exact path="/" component={Landing} />
          <Route path="/login" component={Login} />
          <Route exact path="/profiles" component={ProfileList} />
          <Route path="/profile/:handle" component={ProfileView} />
          <Route path="/register" component={Register} />
          <PrivateRoute path="/add-experience" component={ExperienceAdd} />
          <PrivateRoute path="/add-education" component={EducationAdd} />
          <PrivateRoute path="/create-profile" component={ProfileAdd} />
          <PrivateRoute path="/dashboard" component={Dashboard} />
          <PrivateRoute path="/edit-profile" component={ProfileEdit} />
          <PrivateRoute path="/wall" component={PostView} />
          <PrivateRoute path="/post/:id" component={Post} />
          <Route component={NotFound} />
        </Switch>
        <Footer />
      </div>
    </Router>
  </Provider>
)

export default App
