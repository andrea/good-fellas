/* eslint-disable no-underscore-dangle */
import React from 'react'
import PropTypes from 'prop-types'

import CommentItem from '../CommentItem/CommentItem'

const CommentList = ({ comments, postId }) =>
  comments
    .sort((a, b) => new Date(b.from).getTime() - new Date(a.from).getTime())
    .map(c => <CommentItem key={c._id} comment={c} postId={postId} />)

CommentList.propTypes = {
  comments: PropTypes.array.isRequired,
  postId: PropTypes.string.isRequired,
}

export default CommentList
