import axios from 'axios'
import jwtDecode from 'jwt-decode'

import setAuthToken from '../utils/setAuthToken'
import { GET_ERRORS, SET_CURRENT_USER } from '../types'

// Set current user. Receives decoded user data as payload.
const setCurrentUser = payload => ({
  type: SET_CURRENT_USER,
  payload,
})

// Register user. Receives new user registration data as payload.
const registerUser = (payload, history) => dispatch => {
  axios
    .post('/api/users/register', payload)
    .then(() => history.push('/login'))
    .catch(err => dispatch({ type: GET_ERRORS, payload: err.response.data }))
}

// Login user.
const loginUser = payload => dispatch => {
  axios
    .post('/api/users/login', payload)
    .then(res => {
      // Save to localStorage.
      const { token } = res.data
      // Set token to localStorage.
      localStorage.setItem('jwtToken', token)
      // Set token to Authorization header.
      setAuthToken(token)
      // Decode token to get user data.
      const decoded = jwtDecode(token)
      // Set current user.
      dispatch(setCurrentUser(decoded))
    })
    .catch(err => dispatch({ type: GET_ERRORS, payload: err.response.data }))
}

// Logout user.
const logoutUser = () => dispatch => {
  // Remove token from localStorage.
  localStorage.removeItem('jwtToken')
  // Remove Authorization header for future requests.
  setAuthToken(false)
  // Set isAuthenticated to false by dispatching an empty user object.
  dispatch(setCurrentUser({}))
}

export { registerUser, loginUser, logoutUser, setCurrentUser }
