import axios from 'axios'
import {
  GET_PROFILE,
  PROFILE_LOADING,
  CLEAR_CURRENT_PROFILE,
  GET_ERRORS,
  SET_CURRENT_USER,
  GET_PROFILES,
} from '../types'

// Profile loading
const setProfileLoading = () => ({
  type: PROFILE_LOADING,
})

// Clear profile
const clearCurrentProfile = () => ({
  type: CLEAR_CURRENT_PROFILE,
})

// Get current profile
const getCurrentProfile = () => dispatch => {
  dispatch(setProfileLoading())

  axios
    .get('/api/profile')
    .then(({ data: payload }) => {
      dispatch({ type: GET_PROFILE, payload })
    })
    .catch(() => dispatch({ type: GET_PROFILE, payload: {} }))
}

// Get profile by handle.
const getProfileByHandle = handle => dispatch => {
  dispatch(setProfileLoading())

  axios
    .get(`/api/profile/handle/${handle}`)
    .then(({ data: payload }) => {
      dispatch({ type: GET_PROFILE, payload })
    })
    .catch(() => dispatch({ type: GET_PROFILE, payload: null }))
}

// Create profile.
const createProfile = (userData, history) => dispatch => {
  axios
    .post('/api/profile', userData)
    .then(() => {
      dispatch({ type: GET_ERRORS, payload: {} })
      history.push('/dashboard')
    })
    .catch(err => dispatch({ type: GET_ERRORS, payload: err.response.data }))
}

// Add experience
const addExperience = (expData, history) => dispatch => {
  axios
    .post('/api/profile/experience', expData)
    .then(() => {
      dispatch({ type: GET_ERRORS, payload: {} })
      history.push('/dashboard')
    })
    .catch(err => dispatch({ type: GET_ERRORS, payload: err.response.data }))
}

// Add education
const addEducation = (eduData, history) => dispatch => {
  axios
    .post('/api/profile/education', eduData)
    .then(() => {
      dispatch({ type: GET_ERRORS, payload: {} })
      history.push('/dashboard')
    })
    .catch(err => dispatch({ type: GET_ERRORS, payload: err.response.data }))
}

// Delete both Profile and Account.
const deleteExperience = id => dispatch => {
  axios
    .delete(`/api/profile/experience/${id}`)
    .then(res => dispatch({ type: GET_PROFILE, payload: res.data }))
    .catch(err => dispatch({ type: GET_ERRORS, payload: err.response.data }))
}

// Delete both profile and account.
const deleteEducation = id => dispatch => {
  axios
    .delete(`/api/profile/education/${id}`)
    .then(res => dispatch({ type: GET_PROFILE, payload: res.data }))
    .catch(err => dispatch({ type: GET_ERRORS, payload: err.response.data }))
}

// Delete both profile and account.
const deleteAccount = () => dispatch => {
  axios
    .delete('/api/profile')
    .then(() => dispatch({ type: SET_CURRENT_USER, payload: {} }))
    .catch(err => dispatch({ type: GET_ERRORS, payload: err.response.data }))
}

// Get all profiles
const getProfiles = () => dispatch => {
  dispatch(setProfileLoading())

  axios
    .get('/api/profile/all')
    .then(res => dispatch({ type: GET_PROFILES, payload: res.data }))
    .catch(() => dispatch({ type: GET_PROFILES, payload: null }))
}

export {
  getCurrentProfile,
  getProfileByHandle,
  setProfileLoading,
  clearCurrentProfile,
  createProfile,
  addExperience,
  deleteExperience,
  addEducation,
  deleteEducation,
  deleteAccount,
  getProfiles,
}
