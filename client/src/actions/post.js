import axios from 'axios'

import {
  ADD_POST,
  CLEAR_ERRORS,
  DELETE_POST,
  GET_ERRORS,
  GET_POST,
  GET_POSTS,
  POST_LOADING,
} from '../types'

// Set loading state
const setPostLoading = () => ({ type: POST_LOADING })

// Clear errors
const clearErrors = () => ({ type: CLEAR_ERRORS })

// Add post
const addPost = postData => dispatch => {
  dispatch(clearErrors())
  axios
    .post('/api/posts', postData)
    .then(res => dispatch({ type: ADD_POST, payload: res.data }))
    .catch(err => dispatch({ type: GET_ERRORS, payload: err.response.data }))
}

// Get posts
const getPosts = () => dispatch => {
  axios
    .get('/api/posts')
    .then(res => {
      dispatch(setPostLoading())
      dispatch({ type: GET_POSTS, payload: res.data })
    })
    .catch(() => dispatch({ type: GET_POSTS, payload: null }))
}

// Get post
const getPost = id => dispatch => {
  axios
    .get(`/api/posts/${id}`)
    .then(res => {
      dispatch(setPostLoading())
      dispatch({ type: GET_POST, payload: res.data })
    })
    .catch(() => dispatch({ type: GET_POST, payload: null }))
}

// Delete posts
const deletePost = id => dispatch => {
  axios
    .delete(`/api/posts/${id}`)
    .then(() => dispatch({ type: DELETE_POST, payload: id }))
    .catch(err => dispatch({ type: GET_ERRORS, payload: err.response.data }))
}

// Add like
const addLike = id => dispatch => {
  axios
    .post(`/api/posts/like/${id}`)
    .then(() => dispatch(getPosts()))
    .catch(err => dispatch({ type: GET_ERRORS, payload: err.response.data }))
}

// Remove like
const removeLike = id => dispatch => {
  axios
    .post(`/api/posts/unlike/${id}`)
    .then(() => dispatch(getPosts()))
    .catch(err => dispatch({ type: GET_ERRORS, payload: err.response.data }))
}

// Add comment
const addComment = (postId, commentData) => dispatch => {
  dispatch(clearErrors())
  axios
    .post(`/api/posts/comment/${postId}`, commentData)
    .then(res => dispatch({ type: GET_POST, payload: res.data }))
    .catch(err => dispatch({ type: GET_ERRORS, payload: err.response.data }))
}

// Delete comment
const deleteComment = (postId, commentId) => dispatch => {
  axios
    .delete(`/api/posts/comment/${postId}/${commentId}`)
    .then(res => dispatch({ type: GET_POST, payload: res.data }))
    .catch(err => dispatch({ type: GET_ERRORS, payload: err.response.data }))
}

export {
  addPost,
  getPosts,
  getPost,
  deletePost,
  addLike,
  removeLike,
  addComment,
  deleteComment,
  clearErrors,
}
