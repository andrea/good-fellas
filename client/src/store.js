import { createStore, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunk from 'redux-thunk'

import rootReducer from './reducers'

// Store
const initialState = {}
const middleware = [thunk]
/* eslint-disable no-underscore-dangle */
// noinspection JSUnresolvedVariable
const store = createStore(
  rootReducer,
  initialState,
  composeWithDevTools(applyMiddleware(...middleware))
)
/* eslint-enable */

export default store
