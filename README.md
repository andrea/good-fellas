# The Good Fellas

A Node/React application with the basic functionality of a social network, featuring:

- user profile creation;
- profile listing and viewing;
- basic posting and commenting;
- user dashboard allowing updating and deleting users' own profiles.


## Requirements

To run the application you will need to:

- [install Node.js][Install Node] runtime environment
- [install Docker][Install Docker]
- [Create a new OAuth App][Create OAuth App] from your GitHub account.


## Quick Start

The easiest way to get up and running with The Good Fellas app is by following these steps:

- Clone the repository
- Open Terminal and rename the file `.env.example` to `.env`
- Edit you `.env` file and fill up the empty keys related to your GitHub OAuth App.
- From the project's root folder, install the application by running:  

```bash
npm install
```

- Spin up a dockerized MongoDB instance, by opening a terminal window, changing directory to the project root folder and running:  

```bash
docker-compose up -d
```

- Start both the server and the client applications by running the following command from the Terminal:
  
```bash
npm run app
```

Now head to `http://localhost:3000` and see your running app!

[Install Node]: https://nodejs.org/en/download/
[Install Docker]: https://docs.docker.com/install/
[Create OAuth App]: https://developer.github.com/apps/building-oauth-apps/creating-an-oauth-app/
